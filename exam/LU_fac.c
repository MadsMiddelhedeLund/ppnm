#include"LU_fac.h"

void upper_lu(gsl_matrix* H, gsl_matrix* U, gsl_matrix* L){
  int n = H->size1;
  int m = H->size2;
  // making sure the matrices are diagonal and same size
  assert(n == m);
  assert(U->size1 == n);
  assert(L->size1 == n);
  assert(U->size2 == n);
  assert(L->size2 == n);

  double h,u,l; // elements of the matrices in use

  gsl_matrix_set_identity(L);  // making sure L is the identity, before making sub diagonal l_i;

  for (int i = 0; i < n; i ++ ){
    h = gsl_matrix_get(H,0,i);
    gsl_matrix_set(U,0,i,h);
  }
  for (int i = 0; i < n -1; i ++){
    h = gsl_matrix_get(H,i+1,i);
    u = gsl_matrix_get(U,i,i);
    l = (h+0.0)/u;
    gsl_matrix_set(L,i+1,i,l);
    for (int j = i+1; j < n; j++){
      h = gsl_matrix_get(H,i+1,j);
      u = gsl_matrix_get(U,i,j);
      gsl_matrix_set(U,i+1,j,h-l*u);
    }
  }

}

void lower_lu(gsl_matrix* H, gsl_matrix* U, gsl_matrix* L){
  // transposing the matricies to use upper_lu
  gsl_matrix_transpose(H);
  gsl_matrix* L_upper = gsl_matrix_alloc(U->size1,U->size2);
  gsl_matrix* U_upper = gsl_matrix_alloc(L->size1,L->size2);
  gsl_matrix_set_zero(L_upper);
  gsl_matrix_set_zero(U_upper);

  upper_lu(H,U_upper,L_upper);

  gsl_matrix_transpose(H);
  gsl_matrix_transpose_memcpy(U,L_upper);
  gsl_matrix_transpose_memcpy(L,U_upper);

  gsl_matrix_free(L_upper);
  gsl_matrix_free(U_upper);
}
