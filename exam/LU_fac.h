#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h>
#define RND ((double) rand()/RAND_MAX)

void upper_lu(gsl_matrix* H, gsl_matrix* U, gsl_matrix* L);

void lower_lu(gsl_matrix* H, gsl_matrix* U, gsl_matrix* L);
