#include"LU_fac.h"

void rnd_upper_hessenberg(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      if(j+1 < i) gsl_matrix_set(A,i,j,0);
      else gsl_matrix_set(A,i,j,RND);
    }
  }
}

void rnd_lower_hessenberg(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      if(j-1 > i) gsl_matrix_set(A,i,j,0);
      else gsl_matrix_set(A,i,j,RND);
    }
  }
}

void print_matrix(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  for(int i = 0; i < n;i++){
    for(int j = 0; j < m; j++){
      printf("%7.3f\t",gsl_matrix_get(A,i,j));
    }
    printf("\n");
  }
  printf("\n");
}



void test_upper(int n){

  printf("Test of LU-decomposition of upper hessenberg matrix\n\n");

  gsl_matrix* H = gsl_matrix_alloc(n,n);
  gsl_matrix* L = gsl_matrix_alloc(n,n);
  gsl_matrix* U = gsl_matrix_alloc(n,n);
  gsl_matrix* A = gsl_matrix_alloc(n,n);

  rnd_upper_hessenberg(H);
  gsl_matrix_set_zero(L);
  gsl_matrix_set_zero(U);

  upper_lu(H,U,L);
  printf("A random upper hessenberg matrix\n");
  print_matrix(H);
  printf("The LU-decomposition of the upper hessenberg matrix\n");
  printf("U=\n");
  print_matrix(U);
  printf("L=\n");
  print_matrix(L);
  printf("We see U is upper diagonal and L is lower diagonal\n");


  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,L,U,0,A);
  printf("Test H = L*U = \n");
  print_matrix(A);
  printf("We see this is equal to the origonal hessenberg matrix, H\n");


  gsl_matrix_free(H);
  gsl_matrix_free(L);
  gsl_matrix_free(U);
  gsl_matrix_free(A);

}

void test_lower(int n){
  printf("\nTest of LU-decomposition of lower hessenberg matrix\n\n");


  gsl_matrix* H = gsl_matrix_alloc(n,n);
  gsl_matrix* L = gsl_matrix_alloc(n,n);
  gsl_matrix* U = gsl_matrix_alloc(n,n);
  gsl_matrix* A = gsl_matrix_alloc(n,n);

  rnd_lower_hessenberg(H);
  gsl_matrix_set_zero(L);
  gsl_matrix_set_zero(U);

  lower_lu(H,U,L);
  printf("A random lower hessenberg matrix\n");
  print_matrix(H);
  printf("The LU-decomposition of the lower hessenberg matrix\n");
  printf("U=\n");
  print_matrix(U);
  printf("L=\n");
  print_matrix(L);
  printf("We see U is upper diagonal and L is lower diagonal\n");


  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,L,U,0,A);
  printf("Test H = L*U = \n");
  print_matrix(A);
  printf("We see this is equal to the origonal hessenberg matrix, H \n");


  gsl_matrix_free(H);
  gsl_matrix_free(L);
  gsl_matrix_free(U);
  gsl_matrix_free(A);

}

int main(int argc, char** argv){
  int n=(argc>1? atoi(argv[1]):6);
  test_upper(n);
  test_lower(n);
  return 0;
}
