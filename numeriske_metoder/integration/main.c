#include"integration.h"

int fun_calls;

double fun1(double x){
  fun_calls++;
  return 1./sqrt(x);
}

double fun1_gsl(double x, void* params){
  fun_calls++;
  return 1./sqrt(x);
}

double fun2(double x){
  fun_calls++;
  return sqrt(x);
}

double fun3(double x){
  fun_calls++;
  return 4*sqrt(1-(1-x)*(1-x));
}

double fun3_gsl(double x, void* params){
  fun_calls++;
  return 4*sqrt(1-(1-x)*(1-x));
}

double fun4(double x){
  fun_calls++;
  return exp(-x*x)/sqrt(M_PI);
}

double fun4_gsl(double x, void* params){
  fun_calls++;
  return exp(-x*x)/sqrt(M_PI);
}

double fun5(double x){
  fun_calls++;
  return exp(-x);
}

double fun5_gsl(double x, void* params){
  fun_calls++;
  return exp(-x);
}



void test_a(){

  printf("--------Testing rescursive adaptive interator - Part A --------\n\n");

  double a, b, acc = 1e-4, eps = 1e-4, Q;

  printf("Integrating 1/sqrt(x) from 0 to 1\n");

  a = 0; b = 1; fun_calls = 0;
  Q = adapt(fun1,a,b,acc,eps);
  printf("Q = %g, in %i calls, exact result = %i\n", Q, fun_calls,2);
  printf("eps=acc=%g\n\n",eps);

  printf("Integrating sqrt(x) from 0 to 1\n");

  a = 0; b = 1; fun_calls = 0;
  Q = adapt(fun2,a,b,acc,eps);
  printf("Q = %g, in %i calls\n", Q, fun_calls);
  printf("Checking result, the intgral must be 2/3 = %g\n",2./3);
  printf("eps=acc=%g\n\n",eps);

  printf("Integrating 4*sqrt(1-(1-x)^2) from 0 to 1\n");

  a = 0; b = 1; fun_calls = 0; eps = 1e-10; acc = 1e-10;
  Q = adapt(fun3,a,b,acc,eps);
  printf("Q = %g, in %i calls, eps=acc=%g\n", Q, fun_calls,eps);
  printf("Checking result, Q-M_PI = %g\n",Q-M_PI);
  eps = 1e-11; acc = 1e-11;
  Q = adapt(fun3,a,b,acc,eps);
  printf("Q = %g, in %i calls, eps=acc=%g\n", Q, fun_calls,eps);
  printf("Checking result, Q-M_PI = %g\n",Q-M_PI);
  printf("We see that the difference become 0 with the machine prescicion\n");

}

void test_b(){
  printf("\n--------Testing rescursive adaptive interator with Clenshaw Curtis Transform - Part B --------\n\n");

  double a, b, acc = 1e-4, eps = 1e-4, Q;

  printf("Integrating 1/sqrt(x) from 0 to 1\n");

  a = 0; b = 1; fun_calls = 0;
  Q = clenshaw_curtis(fun1,a,b,acc,eps);
  printf("Q = %g, in %i calls, exact result = %i\n", Q, fun_calls,2);
  printf("We see this improves the function calls with the same precision used,eps=acc=%g\n\n",eps);

  printf("Integrating 4*sqrt(1-(1-x)^2) from 0 to 1\n");

  a = 0; b = 1; fun_calls = 0; eps = 1e-10; acc = 1e-10;
  Q = clenshaw_curtis(fun3,a,b,acc,eps);
  printf("Q = %10.20g, in %i calls, eps=acc=%g\n", Q, fun_calls,eps);
  printf("Checking result, Q-M_PI = %10.20g\n",Q-M_PI);
  eps = 1e-11; acc = 1e-11;
  Q =clenshaw_curtis(fun3,a,b,acc,eps);
  printf("Q = %10.20g, in %i calls, eps=acc=%g\n", Q, fun_calls,eps);
  printf("Checking result, Q-M_PI = %g\n",Q-M_PI);
  printf("We see that the difference become 0 with the machine prescicion\n");
  printf("On my machine the clenshaw curtis transform requires more calls but in the same order of magnitude\n\n");

  printf("Testing GSL-routines\n\n");

  printf("Integrating 1/sqrt(x) from 0 to 1\n");

  double result, abserr; eps = 1e-4; acc = 1e-4;
  fun_calls = 0;
  gsl_function F;
  F.function = fun1_gsl;
  F.params = NULL;
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100);
  gsl_integration_qags(&F, a,b, eps, acc, 100, w, &result, &abserr);
  printf("GSL-routine yields result with Q = %g, which is found in %i function calls\n\n",result, fun_calls);

  printf("Integrating 4*sqrt(1-(1-x)^2) from 0 to 1\n");

  eps = 1e-10; acc = 1e-10;
  fun_calls = 0;
  F.function = fun3_gsl;
  F.params = NULL;
  gsl_integration_qags(&F, a,b, eps, acc, 100, w, &result, &abserr);
  printf("GSL-routine yields result with Q = %10.20g, which is found in %i function calls, with epsabs=epsrel=%g\n",result, fun_calls,eps);
  printf("Checking result, Q-M_PI = %g\n",result-M_PI);

  gsl_integration_workspace_free(w);
}

void test_c(){

  printf("\n--------Testing rescursive adaptive interator with indefinite limits - Part C --------\n\n");

  double a, b, acc = 1e-6, eps = 1e-6, Q;

  printf("Integrating exp(-x^2)/sqrt(pi) from -infinity to infinity\n");

  a = -INFINITY; b = INFINITY; fun_calls = 0;
  Q = adapt(fun4,a,b,acc,eps);
  printf("Q = %g, in %i calls, exact result = %i\n", Q, fun_calls,1);
  printf("eps=acc=%g\n\n",eps);



  printf("Integrating exp(-x) from 0 to infinity\n");

  a = 0.; b = INFINITY; fun_calls = 0;
  Q = adapt(fun5,a,b,acc,eps);
  printf("Q = %g, in %i calls, exact result = %i\n", Q, fun_calls,1);
  printf("eps=acc=%g\n\n",eps);

  printf("Testing GSL-routines\n\n");

  printf("Integrating exp(-x*x/2)/sqrt(pi) from -inf to infs\n");

  double result, abserr;
  fun_calls = 0;
  gsl_function F;
  F.function = fun4_gsl;
  F.params = NULL;
  gsl_integration_workspace* w = gsl_integration_workspace_alloc(100);
  gsl_integration_qagi(&F, eps, acc, 100, w, &result, &abserr);
  printf("GSL-routine yields result with Q = %g, which is found in %i function calls\n",result, fun_calls);
  printf("eps=epsrel=%g\n\n",eps);

  printf("Integrating exp(-x) from 0 to inf\n");

  fun_calls = 0;
  F.function = fun5_gsl;
  F.params = NULL;
  gsl_integration_qagiu(&F, a, eps, acc, 100, w, &result, &abserr);
  printf("GSL-routine yields result with Q = %g, which is found in %i function calls\n",result, fun_calls);
  printf("eps=acc=%g\n\n",eps);

  gsl_integration_workspace_free(w);
}

int main(){
  test_a();
  test_b();
  test_c();
  return 0;
}
