#include<math.h>
#include<assert.h>
#include<stdio.h>
#include<gsl/gsl_integration.h>
#include <float.h>

double transform(double f(double), double x, int type, double limit);

double adapt24(double f(double), double a, double b, double acc, double eps,
double f2, double f3, int nrec, int type, double limit);

double adapt(double f(double), double a, double b, double acc, double eps);

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps);
