#include"integration.h"
double transform(double f(double), double x, int type, double limit){
  if(type == 0) return f(x); // just normal limits
  if(type == 1) return (f((1-x)/x)+f(-(1-x)/x))/(x*x);
  if(type == 2) return f(limit+(1-x)/x)/(x*x);
  if(type == 3) return f(limit-(1-x)/x)/(x*x);
  else{
    fprintf(stderr,"Error in transformation, not accpected type");
  }assert(0);
}

double adapt24(double f(double), double a, double b, double acc, double eps,
double f2, double f3, int nrec, int type, double limit){
  assert(nrec<1000000);
  double f1 = transform(f,a+(b-a)/6,type,limit);
  double f4 = transform(f,a+5*(b-a)/6,type,limit);
  double Q = (2*f1+f2+f3+2*f4)/6*(b-a), q = (f1+f2+f3+f4)/4*(b-a);
  double tau = acc+eps*fabs(Q), err = fabs(Q-q); // tau = tolerance
  if(err < tau) return Q;
  else {
    double Q1 = adapt24(f,a,(a+b)/2,acc/sqrt(2.),eps,f1,f2,nrec+1,type,limit);
    double Q2 = adapt24(f,(a+b)/2,b,acc/sqrt(2.),eps,f3,f4,nrec+1,type,limit);
    return Q1+Q2;
  }
}

double adapt(double f(double), double a, double b, double acc, double eps){
  int nrec = 0, type;
  double limit = 0;
  if(a>b) return -adapt(f,b,a,acc,eps);
  if(isinf(a) == 0 && isinf(b) == 0) type = 0;
  if(isinf(-a) == 1 && isinf(b) == 1){type = 1; a=0; b=1;}
  if(isinf(a) == 0 && isinf(b) == 1){type = 2;limit = a; a=0; b=1;}
  if(isinf(-a) == 1 && isinf(b) == 0){type = 3; limit = b; a=0; b=1;}
  double f2 = transform(f, a + 2*(b-a)/6, type, limit);
  double f3 = transform(f, a + 4*(b-a)/6, type, limit);
  return adapt24(f,a,b,acc,eps,f2,f3,nrec,type,limit);

}

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps){
  double g(double t){return f( (a+b)/2+(a-b)/2*cos(t) )*sin(t)*(b-a)/2;}
  return adapt(g,0,M_PI,acc,eps);
}
