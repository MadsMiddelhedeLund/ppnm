#include"ode.h"
void sin_ode(double t, gsl_vector* y, gsl_vector* dydt){
  double y1 = gsl_vector_get(y,0);
  double y2 = gsl_vector_get(y,1);
  double dydt1 = y2;
  double dydt2 = -y1;
  gsl_vector_set(dydt,0,dydt1);
  gsl_vector_set(dydt,1,dydt2);
}

void exp_ode(double t, gsl_vector*y, gsl_vector* dydt){
  double y1 = gsl_vector_get(y,0);
  double dydt1 = y1;
  gsl_vector_set(dydt,0,dydt1);
}

double my_sin(double x){
  return sin(x);
}

void test_exp_a(){
  printf("\n\nPart A - solving y = dy/dx\n");
  int dim = 1;
  gsl_vector* y = gsl_vector_alloc(dim);

  double h, t, eps = 1e-4, acc = 1e-4;
  FILE *file = fopen("exp_a.dat","w");
  for (double x = 0; x < 3; x+=0.1){
    t = 0;
    gsl_vector_set(y,0,1);
    h = 0.01*x/fabs(x);

    driver(&t, x, &h, y,eps, acc,rkstep12,exp_ode);

    fprintf(file,"%g %g\n", t, gsl_vector_get(y,0));
  }
  gsl_vector_free(y);
  fclose(file);
}
void test_sin_a(){
    printf("\n\nPart A - solving y = -dy^2/dx^2\n");
  int dim = 2;
  gsl_vector* y = gsl_vector_alloc(dim);

  double h, t, eps = 1e-4, acc = 1e-4;
  int no_data = 100;
  FILE *file = fopen("sin_a.dat","w");
  for (double x = -2*M_PI; x < 2*M_PI; x+=4*M_PI/no_data){
    t = 0;
    gsl_vector_set(y,0,0);
    gsl_vector_set(y,1,1);
    h = 0.01*x/fabs(x);

    driver(&t, x, &h, y,eps, acc,rkstep12,sin_ode);

    fprintf(file,"%g %g %g\n", t, gsl_vector_get(y,0),gsl_vector_get(y,1));
  }
  gsl_vector_free(y);
  fclose(file);
}

void test_b(){
  printf("\n\nPart B - solving y = -dy^2/dx^2, while storing the path\n");
  int dim = 2;
  gsl_vector* y = gsl_vector_alloc(dim);

  double h, t, b, eps = 1e-2, acc = 1e-2;
  t = 0;
  b = 2*M_PI;
  h = 0.1;
  gsl_vector_set(y, 0, 0.);
  gsl_vector_set(y, 1, 1.);
  gsl_matrix* path = gsl_matrix_alloc(1000,3);
  int num_saved;

  driver_with_path(&t,b,&h,y,eps,acc,rkstep12,sin_ode,path,&num_saved);

  FILE* file = fopen("sin_b.dat","w");
  double y1;
  for(int i = 0; i < num_saved; i++){
    y1 = 0; // DONT TAKE THIS AWAY IT WILL FUCK YOU UP
    fprintf(file,"%g %g %g \n",gsl_matrix_get(path,i,0),gsl_matrix_get(path,i,1)+y1,gsl_matrix_get(path,i,2));
  }
  fclose(file);
  gsl_vector_free(y);
  gsl_matrix_free(path);
}

void test_c(){ // virker med rkstep12 men ikke med rkstep34
  printf("\n\nPart C - int 0 to x sin(x')dx' = 1-cos(x)\n");

  double t, h, x_end, result, eps = 1e-5, acc = 1e-5;
  x_end= 2*M_PI;
  FILE* file = fopen("int_sin.dat","w");
  for (double x = 0; x < x_end; x += 0.1) {
        t = 0;
        h = 0.01;
        integration_ode(&t, x, &h, eps, acc, rkstep12, my_sin, &result);
        fprintf(file, "%g %g\n", t, result);
    }
}

int main(){
  test_sin_a();
  test_exp_a();
  test_b();
  test_c();
  return 0;
}
