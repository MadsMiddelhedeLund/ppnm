#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void rkstep34(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err);

void driver(double* t, double b, double* h, gsl_vector* yt, double acc, double eps,
void stepper(double y, double h, gsl_vector* yt,
    void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err),
void f(double t, gsl_vector* y, gsl_vector* dydt));

void driver_with_path(double* t, double b, double* h, gsl_vector* yt, double acc, double eps,
void stepper(double y, double h, gsl_vector* yt,
    void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err),
void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_matrix* path, int* num_saved);

void integration_ode(double* t, double x, double* h, double acc, double eps,
void stepper(double y, double h, gsl_vector* yt,
    void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err),
double fun(double), double* result);

void rkstep12(double t, double h, gsl_vector* y, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yh, gsl_vector* err);

void vector_sum(gsl_vector* x, double a, gsl_vector* y, double b);
