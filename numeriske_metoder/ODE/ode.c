#include "ode.h"

// doesn't work
void rkstep34(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err){
  int n = yt->size;
  gsl_vector* kp = gsl_vector_alloc(n);
  gsl_vector* kpm1 = gsl_vector_alloc(n);
  gsl_vector* ktemp = gsl_vector_alloc(n);
  gsl_vector* dydt = gsl_vector_alloc(n);
  // p order
  f(t,yt,ktemp);                // k0
  gsl_blas_daxpy(1./6,ktemp,kp);// k
  gsl_vector_memcpy(yth,yt);    //y = y0
  gsl_blas_daxpy(0.5*h,ktemp,yth);// y = y+0.5*h*k0
  f(t+0.5*h,yth,ktemp);           // k1 = f(x0+0.5*h,y)
  gsl_blas_daxpy(1./3,ktemp,kp);
  gsl_vector_memcpy(yth,yt);
  gsl_blas_daxpy(0.5*h,ktemp,yth);
  f(t+0.5*h,yth,ktemp);
  gsl_blas_daxpy(1./3,ktemp,kp);
  gsl_vector_memcpy(yth,yt);
  gsl_blas_daxpy(h,ktemp,yth);
  f(t+h,yth,ktemp);
  gsl_blas_daxpy(1./6,ktemp,kp);

  // p-1 order
  f(t,yt,ktemp);
  gsl_blas_daxpy(1./6,ktemp,kpm1);
  gsl_vector_memcpy(yth,yt);
  gsl_blas_daxpy(0.5*h,ktemp,yth);
  f(0.5*h+t,yth,ktemp);
  gsl_blas_daxpy(4./6,ktemp,kpm1);
  f(t,yt,ktemp);
  gsl_vector_memcpy(yth,yt);
  gsl_blas_daxpy(h,ktemp,yth);
  f(t+h,yth,ktemp);
  gsl_blas_daxpy(1./6,ktemp,kpm1);

  // calculate yth
  gsl_vector_memcpy(yth,yt);
  gsl_blas_daxpy(h,kp,yth);

  // calculate error
  gsl_vector_memcpy(err,yt);
  gsl_blas_daxpy(h,kpm1,err);
  gsl_blas_daxpy(-1,yth,err);
  gsl_vector_scale(err,-1);

  // free stuff
  gsl_vector_free(kp);
  gsl_vector_free(kpm1);
  gsl_vector_free(ktemp);
  gsl_vector_free(dydt);

}

// taken from text
void rkstep12(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err){
      int n = yt->size;
      gsl_vector* k0 = gsl_vector_alloc(n);
      gsl_vector* yx = gsl_vector_alloc(n);
      gsl_vector* k12 = gsl_vector_alloc(n);
      f(t,yt,k0);
      for (int i = 0; i < n; i++){
        gsl_vector_set(yx,i,h/2*gsl_vector_get(k0,i)+gsl_vector_get(yt,i));
      }
      f(t+h/2,yx,k12);
      for (int i = 0; i < n; i++){
        gsl_vector_set(yth,i,h*gsl_vector_get(k12,i)+gsl_vector_get(yt,i));
      }
      for(int i = 0; i < n; i++){
        gsl_vector_set(err,i,(gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2);
      }
      gsl_vector_free(k0);
      gsl_vector_free(k12);
      gsl_vector_free(yx);
}

void driver(double* t, double b, double* h, gsl_vector* yt, double acc, double eps,
void stepper(double y, double h, gsl_vector* yt,
    void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err),
void f(double t, gsl_vector* y, gsl_vector* dydt)){

  if (fabs(*t + *h) > fabs(b)) {*h = b - *t;}

  int n = yt->size;
  gsl_vector* yth = gsl_vector_alloc(n);
  gsl_vector* err = gsl_vector_alloc(n);

  int num_iter = 0;
  int iter_max = 1e9;
  double err_norm, y_norm, tau, a = *t;

  do{
    num_iter++;

    stepper(*t, *h, yt,f,yth,err);

    err_norm = gsl_blas_dnrm2(err);
    y_norm = gsl_blas_dnrm2(yt);

    tau = (eps*y_norm+acc)*sqrt(*h/(b-a));

    if (tau > err_norm){
      gsl_vector_memcpy(yt,yth);
      *t = *t + *h;
    }

    *h *= pow(tau/err_norm,0.25)*0.95;

    if(fabs(*t + *h )>fabs(b)) {*h = b-*t;}
  }while(num_iter < iter_max && fabs(*t -b)> 1e-12);
  }

void driver_with_path(double* t, double b, double* h, gsl_vector* yt, double acc, double eps,
void stepper(double y, double h, gsl_vector* yt,
    void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err),
void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_matrix* path, int* num_saved){


    if (fabs(*t + *h) > fabs(b)) {*h = b - *t;}

    int n = yt->size;
    gsl_vector* yth = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);

    int num_iter = 0;
    int iter_max = 1e6;
    double err_norm, y_norm, tau, a = *t;

    do{
      num_iter++;

      stepper(*t, *h, yt,f,yth,err);

      err_norm = gsl_blas_dnrm2(err);
      y_norm = gsl_blas_dnrm2(yt);

      tau = (eps*y_norm+acc)*sqrt(*h/(b-a));

      if (tau > err_norm){
        gsl_vector_memcpy(yt,yth);
        *t = *t + *h;

        if(*num_saved < path->size1){
          gsl_matrix_set(path, *num_saved,0,*t);
          for (int i = 0; i < (yt->size); ++i) {
            gsl_matrix_set(path, *num_saved, i+1, gsl_vector_get(yt, i));
          }
        }
        (*num_saved)++;
      }

      *h *= pow(tau/err_norm,0.25)*0.95;

      if(fabs(*t + *h )>fabs(b)) {*h = b-*t;}
    }while(num_iter < iter_max && fabs(*t -b)> 1e-12);
}

void integration_ode(double* t, double x, double* h, double acc, double eps,
void stepper(double y, double h, gsl_vector* yt,
    void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err),
double fun(double), double* result){

    void int_fun(double t, gsl_vector* y, gsl_vector* dydt){
      gsl_vector_set(dydt, 0, fun(t));
    }

    gsl_vector* y = gsl_vector_alloc(1);
    gsl_vector_set(y, 0., 0.);

    driver(t, x, h, y, acc, eps, stepper, int_fun);

    *result = gsl_vector_get(y, 0);

    gsl_vector_free(y);
}
