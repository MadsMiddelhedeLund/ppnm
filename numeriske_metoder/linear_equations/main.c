#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"
#define RND ((double) rand()/RAND_MAX)

void rnd_matrix_generator(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      gsl_matrix_set(A,i,j,RND);
    }
  }
}


void rnd_vector_generator(gsl_vector* v){
  int n = v->size;
  for(int i = 0; i < n;i++){
    gsl_vector_set(v,i,RND);
  }
}

void print_matrix(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  for(int i = 0; i < n;i++){
    for(int j = 0; j < m; j++){
      printf("%7.3f\t",gsl_matrix_get(A,i,j));
    }
    printf("\n");
  }
  printf("\n");
}

void print_vector(gsl_vector* v){
  int n = v->size;
  for(int i = 0; i < n; i++){
    printf("% 7.3f\n",gsl_vector_get(v,i));
  }
  printf("\n");
}

void qr_gs_decomp_test(){
  printf("----Exercise 1.1-----\n");

  // inititalizing matrix
  int n = 4;
  int m = 3;
  gsl_matrix* A = gsl_matrix_alloc(n,m);
  rnd_matrix_generator(A);
  printf("A=\n");
  print_matrix(A);

  gsl_matrix* R = gsl_matrix_alloc(m,m);
  qr_gs_decomp(A,R);

  printf("R=\n");
  print_matrix(R);

  gsl_matrix* B = gsl_matrix_alloc(m,m);
  gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,B); // C = Q^T*Q
  printf("(Q^T)*Q=\n");
  print_matrix(B);

  gsl_matrix* C = gsl_matrix_alloc(n,m);
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,C);
  printf("Q*R=\n");
  print_matrix(C);

  gsl_matrix_free(A);
  gsl_matrix_free(R);
  gsl_matrix_free(B);
}

void qr_gs_solve_test(){
  printf("----Exercise 1.2----\n");
  int n = 4;
  int m = n;
  gsl_matrix* A = gsl_matrix_alloc(n,m);
  gsl_vector* b = gsl_vector_alloc(n);
  gsl_matrix* R = gsl_matrix_alloc(n,m);
  gsl_vector* x = gsl_vector_alloc(n);
  gsl_matrix* Q = gsl_matrix_alloc(n,m);

  rnd_matrix_generator(A);
  rnd_vector_generator(b);
  gsl_matrix_memcpy(Q,A);

  printf("A=\n");
  print_matrix(A);
  printf("b=\n");
  print_vector(b);

  qr_gs_decomp(Q,R);
  qr_gs_solve(Q,R,b,x);
  printf("Solution to A*x=Q*R*x=b\n");
  print_vector(x);
  print_matrix(Q);

  gsl_vector* c = gsl_vector_alloc(n);
  gsl_blas_dgemv(CblasNoTrans,1.0,A,x,0.0,c);
  printf("A*x=?\n");
  print_vector(c);

  gsl_matrix_free(A);
  gsl_matrix_free(R);
  gsl_matrix_free(Q);
  gsl_vector_free(b);
  gsl_vector_free(x);
  gsl_vector_free(c);
}

void qr_gs_inverse_test(){
  printf("----Exercise 2----\n");
  int n = 4;
  gsl_matrix* A = gsl_matrix_alloc(n,n);
  gsl_matrix* R = gsl_matrix_alloc(n,n);
  gsl_matrix* Q = gsl_matrix_alloc(n,n);
  gsl_matrix* B = gsl_matrix_alloc(n,n);

  rnd_matrix_generator(A);
  gsl_matrix_memcpy(Q,A);
  qr_gs_decomp(Q,R);
  qr_gs_inverse(Q,R,B);

  printf("A=\n");
  print_matrix(A);
  printf("Inverse of A, A^-1=\n");
  print_matrix(B);

  gsl_matrix* C = gsl_matrix_alloc(n,n);

  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,B,0.0,C);
  printf("A*A^-1=\n");
  print_matrix(C);
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,B,A,0.0,C);
  printf("A^-1*A=\n");
  print_matrix(C);
  gsl_matrix_free(A);
  gsl_matrix_free(R);
  gsl_matrix_free(Q);
  gsl_matrix_free(B);
  gsl_matrix_free(C);

}

int main(){
  qr_gs_decomp_test();
  qr_gs_solve_test();
  qr_gs_inverse_test();
return 0;
}
