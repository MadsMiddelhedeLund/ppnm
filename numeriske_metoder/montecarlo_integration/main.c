#include"mc_int.h"

double fun1(gsl_vector* x){
  double x1 = gsl_vector_get(x,0);
  double x2 = gsl_vector_get(x,1);
  return exp(-(x1*x1+x2*x2))/(M_PI);
}

double fun2(gsl_vector* x){
  double x1 = gsl_vector_get(x,0);
  double x2 = gsl_vector_get(x,1);
  double x3 = gsl_vector_get(x,2);
  return 1 / (1 - cos(x1)*cos(x2)*cos(x3)) / pow(M_PI,3);
}

void test_a(){
  printf("\n\n\n----- Part A - Testing integrals with monte carlo ------\n\n");
  int N = 1e6; // no of points
  printf("integrating exp(-(x^2+y^2))/pi from x=-1 to 1 and y = -1 to 1\n");

  gsl_vector* a1 = gsl_vector_alloc(2);
  gsl_vector* b1 = gsl_vector_alloc(2);
  gsl_vector_set(a1,0,-1);
  gsl_vector_set(a1,1,-1);
  gsl_vector_set(b1,0,1);
  gsl_vector_set(b1,1,1);
  double result, err;
  plainmc(a1,b1,fun1,N,&result,&err);

  printf("Monte Carlo integrator yields I = %g +- %g, using %i points\n",result,err, N);
  printf("The exact result would be erf(1)^2 = %g\n\n",erf(1)*erf(1));

  printf("integrating [1-cos(x)*cos(y)*cos(z)]^-1/pi^3 from x=0 to pi and y = 0 to pi and z = 0 to pi\n");

  gsl_vector* a2 = gsl_vector_alloc(3);
  gsl_vector* b2 = gsl_vector_alloc(3);
  gsl_vector_set(a2,0,0.);
  gsl_vector_set(a2,1,0.);
  gsl_vector_set(a2,2,0.);
  gsl_vector_set(b2,0,M_PI);
  gsl_vector_set(b2,1,M_PI);
  gsl_vector_set(b2,2,M_PI);
  plainmc(a2,b2,fun2,N,&result,&err);

  printf("Monte Carlo integrator yields I = %g +- %g, using %i points\n",result,err, N);
  printf("The exact result would be gamma(1/4)^4/(4*pi^3) = %g\n",1.3932039296856768591842462603255);

}

void test_b(){

  printf("\n\n----- Part B - Testing error of monte carlo integration ------\n\n");
  printf("integrating exp(-(x^2+y^2))/pi from x=-1 to 1 and y = -1 to 1 with different N from 1e3 to 1e6 in steps of 1e3\n");

  gsl_vector* a = gsl_vector_alloc(2);
  gsl_vector* b = gsl_vector_alloc(2);
  gsl_vector_set(a,0,-1);
  gsl_vector_set(a,1,-1);
  gsl_vector_set(b,0,1);
  gsl_vector_set(b,1,1);
  double result, err;
  FILE* file = fopen("error_mc.dat","w");

  for (int N=1e3; N < 1e5; N+=1e3){
    plainmc(a,b,fun1,N,&result,&err);
    fprintf(file,"%i %g\n",N,err);
  }

  gsl_vector_free(a);
  gsl_vector_free(b);
  fclose(file);
}


int main(){
  test_a();
  test_b();
  return 0;
}
