#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#define RND ((double)rand()/RAND_MAX)

void randomx(gsl_vector* a, gsl_vector* b, gsl_vector* x);

void plainmc(gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N, double* result, double* err);
