#include"mc_int.h"

void randomx(gsl_vector* a, gsl_vector* b, gsl_vector* x){
  int dim = a->size;
  for(int i = 0; i < dim; i++){
    double x_i = gsl_vector_get(a,i)+RND*(gsl_vector_get(b,i)-gsl_vector_get(a,i));
    gsl_vector_set(x,i,x_i);
  }
}

void plainmc(gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N, double* result, double* err){
  int dim = a->size;
  double V = 1;
  for(int i = 0; i < dim; i++) V *= gsl_vector_get(b,i)-gsl_vector_get(a,i);

  double sum = 0, sum2 = 0, fx;
  gsl_vector* x = gsl_vector_alloc(dim);

  for( int i = 0; i < N; i++){
    randomx(a,b,x);
    fx = f(x);
    sum += fx;
    sum2 += fx*fx;
  }

  double avr = sum/N, var = sum2/N-avr*avr;
  *result = avr*V; *err = sqrt(var/N)*V;

  // free stuff
  gsl_vector_free(x);
}
