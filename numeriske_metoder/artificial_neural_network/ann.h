#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h>

typedef struct {
	int n;
	double(*f)(double);
	gsl_vector* data;
	} ann;

ann* ann_alloc(int n, double (*f)(double));

void ann_free(ann* network);

void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist, double dx, double eps);

double ann_feed_forward(ann* network, double x);

double ann_feed_forward_deri(ann* network, double x);

double ann_feed_forward_antideri(ann* network, double x,double x_start);

void numeric_gradient(double f(gsl_vector*), gsl_vector*x, gsl_vector*grad_f,double dx);

void broyden_update(double f(gsl_vector* x), gsl_matrix* B, gsl_vector* z,gsl_vector* x, gsl_vector* gz, gsl_vector* df, gsl_vector* y, gsl_vector* Dx, gsl_vector* u, double lambda, double lambda_eps, double dx);

int qnewton_broyden_num(double f(gsl_vector* x),gsl_vector* x, double dx, double eps);

double transform(double f(double), double x, int type, double limit);

double adapt24(double f(double), double a, double b, double acc, double eps,
double f2, double f3, int nrec, int type, double limit);

double adapt(double f(double), double a, double b, double acc, double eps);
