#include"ann.h"

ann* ann_alloc(int n, double (*f)(double)){
  ann* network = malloc(sizeof(ann));
  network -> n = n; // number of neurons
  network -> f = f; // activation function
  network -> data = gsl_vector_alloc(3*n);
  return network;
}

void ann_free(ann* network){
  gsl_vector_free(network->data);
  free(network);
}

double ann_feed_forward(ann* network, double x){
  double sum = 0;
  int n = network -> n;
  double (*f)(double) = network -> f;
  gsl_vector* data = network -> data;

  for(int i = 0; i < n; i++){
    double a = gsl_vector_get(data,3*i+0);
    double b = gsl_vector_get(data,3*i+1);
    double w = gsl_vector_get(data,3*i+2);
    sum += f((x-a)/b)*w;
  }

  return sum;
}

double ann_feed_forward_deri(ann* network, double x){
  double sum = 0;
  int n = network -> n;
  double (*f)(double) = network -> f;
  gsl_vector* data = network -> data;

  // making input to numeric_gradient
  gsl_vector* x_vec = gsl_vector_alloc(1);
  gsl_vector* df = gsl_vector_alloc(1);
  double dx = 1e-8;

  double f_vec(gsl_vector* x){
    return f(gsl_vector_get(x,0));
  }

  for(int i = 0; i < n; i++){
    double a = gsl_vector_get(data,3*i+0);
    double b = gsl_vector_get(data,3*i+1);
    double w = gsl_vector_get(data,3*i+2);
    gsl_vector_set(x_vec,0,(x-a)/b);
    numeric_gradient(f_vec,x_vec,df,dx);
    sum += gsl_vector_get(df,0)*w/b;
  }

  return sum;
}

double ann_feed_forward_antideri(ann* network, double x,double x_start){
  double sum = 0;
  int n = network -> n;
  double (*f)(double) = network -> f;
  gsl_vector* data = network -> data;
  double eps = 1e-6, acc = 1e-6;

  for(int i = 0; i < n; i++){
    double a = gsl_vector_get(data,3*i+0);
    double b = gsl_vector_get(data,3*i+1);
    double w = gsl_vector_get(data,3*i+2);
    sum += w*b*adapt(f,(x_start-a)/b,(x-a)/b,eps,acc);
  }

  return sum;
}

void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist, double dx, double eps){

  gsl_vector* data = network -> data;
  int n = network -> n;

  double delta(gsl_vector* p){
    gsl_vector_memcpy(data,p);
    double sum = 0;
    int N = xlist ->size;
    for(int i = 0; i < N; i++){
      double x = gsl_vector_get(xlist,i);
      double y = gsl_vector_get(ylist,i);
      double f = ann_feed_forward(network,x);
      sum += (f-y)*(f-y);
    }
    return sum/N;
  }
  int size = 3*n;
  gsl_vector* p = gsl_vector_alloc(size);
  gsl_vector_memcpy(p,data);
  qnewton_broyden_num(delta,p,dx,eps);
  gsl_vector_memcpy(data,p);
  gsl_vector_free(p);
}

// I tried to implement something which could train the neural network to solve diff equations. 
/*
void ann_train_diff(ann* network, double dx, double eps, double c, double a, double b,double yc, double yc_prime, double diffeq(ann* network, double x)){

  gsl_vector* data = network -> data;
  int n = network -> n;
  double diffeq_value (double x){
    return diffeq(network,x);
  }
  double delta(gsl_vector* p){
    double delta_p;
    gsl_vector_memcpy(data,p);
    double f = ann_feed_forward(network,c);
    double df = ann_feed_forward_deri(network,c)
    double F = adapt(diffeq_value,a,b,eps,eps); // det eneste der skal laves er en function der tager F_P, smider ind i diff ligning og derefter smider et tal ud. Men kun tager et tal og så skal diff eq laves
    double delta_p= fabs(f-yc)*(b-a)+fabs(df-yc_prime)*(b-a)+F;
    return deltaP;
  }
  int size = 3*n;
  gsl_vector* p = gsl_vector_alloc(size);
  gsl_vector_memcpy(p,data);
  qnewton_broyden_num(delta,p,dx,eps);
  gsl_vector_memcpy(data,p);
  gsl_vector_free(p);
}
*/

// algorithms from minimization

void numeric_gradient(double f(gsl_vector*), gsl_vector*x, gsl_vector*grad_f,double dx){
  double fx = f(x);
   for (int i = 0; i < grad_f->size; i++) {
       double x_i = gsl_vector_get(x, i);
       gsl_vector_set(x, i, x_i + dx);
       double fx_dx = f(x);
       gsl_vector_set(x, i, x_i);
       double grad_f_i = (fx_dx - fx) / dx;
       gsl_vector_set(grad_f, i, grad_f_i);
	}
}

void broyden_update(double f(gsl_vector* x), gsl_matrix* B, gsl_vector* z,gsl_vector* x, gsl_vector* gz, gsl_vector* df, gsl_vector* y, gsl_vector* Dx, gsl_vector* u, double lambda, double lambda_eps, double dx){
  if(lambda > lambda_eps){
    numeric_gradient(f,z,gz,dx);
    gsl_vector_memcpy(y,gz);
    gsl_blas_daxpy(-1,df,y); /* y=grad(z)-grad(x) */
    gsl_vector_memcpy(u,Dx); /* u=s */
    gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); /* u=s-By */
    double sTy,uTy;
    gsl_blas_ddot(Dx,y,&sTy);
    if(fabs(sTy)>1e-12){
      gsl_blas_ddot(u,y,&uTy);
      double gamma=uTy/2/sTy;
      gsl_blas_daxpy(-gamma,Dx,u); /* u=u-gamma*s */
      gsl_blas_dger(1.0/sTy,u,Dx,B);
      gsl_blas_dger(1.0/sTy,Dx,u,B);
    }
  }
  else{
    gsl_matrix_set_identity(B);
  }
}
int qnewton_broyden_num(double f(gsl_vector* x),gsl_vector* x, double dx, double eps){
  int n = x-> size;
  int iter = 0;
  int iter_max = 1e3;
  double lambda_eps = 1e-6;


  double lambda, norm_df, fx, alpha,DxTdf;

  gsl_matrix* B = gsl_matrix_alloc(n,n);
  gsl_vector* df = gsl_vector_alloc(n);
  gsl_vector* u = gsl_vector_alloc(n);
  gsl_vector* y = gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n);
  gsl_vector* z = gsl_vector_alloc(n);
  gsl_vector* gz = gsl_vector_alloc(n);
  gsl_matrix_set_identity(B);
  numeric_gradient(f,x,df,dx);
  fx = f(x);
  while(1){
    iter ++;

    // finding Dx
    gsl_blas_dgemv(CblasNoTrans,-1,B,df,0,Dx);

    lambda = 1;
    alpha = 1e-2;

    DxTdf = gsl_blas_ddot(Dx,df,&DxTdf);
    // back tracking
    while (1) {
            gsl_vector_memcpy(z,x);
            gsl_vector_add(z, Dx); //x = x - lambda*Delta_x
            if(f(z) < (fx + alpha*lambda*DxTdf) || lambda < lambda_eps) break;
            lambda *=0.5;
            gsl_vector_scale(Dx,0.5);
        }
    norm_df = gsl_blas_dnrm2(df);

    if (norm_df < eps || iter > iter_max) {break;}
    else{
      broyden_update(f,B,z,x,gz,df,y,Dx,u,lambda,lambda_eps,dx);
      gsl_vector_memcpy(x,z);
      gsl_vector_memcpy(df,gz);
      fx=f(x);
    }
  }

  //free stuff
  gsl_matrix_free(B);
  gsl_vector_free(df);
  gsl_vector_free(u);
  gsl_vector_free(y);
  gsl_vector_free(Dx);
  gsl_vector_free(z);
  gsl_vector_free(gz);

  return iter;
}

// integration

double transform(double f(double), double x, int type, double limit){
  if(type == 0) return f(x); // just normal limits
  if(type == 1) return (f((1-x)/x)+f(-(1-x)/x))/(x*x);
  if(type == 2) return f(limit+(1-x)/x)/(x*x);
  if(type == 3) return f(limit-(1-x)/x)/(x*x);
  else{
    fprintf(stderr,"Error in transformation, not accpected type");
  }assert(0);
}

double adapt24(double f(double), double a, double b, double acc, double eps,
double f2, double f3, int nrec, int type, double limit){
  assert(nrec<1000000);
  double f1 = transform(f,a+(b-a)/6,type,limit);
  double f4 = transform(f,a+5*(b-a)/6,type,limit);
  double Q = (2*f1+f2+f3+2*f4)/6*(b-a), q = (f1+f2+f3+f4)/4*(b-a);
  double tau = acc+eps*fabs(Q), err = fabs(Q-q); // tau = tolerance
  if(err < tau) return Q;
  else {
    double Q1 = adapt24(f,a,(a+b)/2,acc/sqrt(2.),eps,f1,f2,nrec+1,type,limit);
    double Q2 = adapt24(f,(a+b)/2,b,acc/sqrt(2.),eps,f3,f4,nrec+1,type,limit);
    return Q1+Q2;
  }
}

double adapt(double f(double), double a, double b, double acc, double eps){
  int nrec = 0, type;
  double limit = 0;
  if(a>b) return -adapt(f,b,a,acc,eps);
  if(isinf(a) == 0 && isinf(b) == 0) type = 0;
  if(isinf(-a) == 1 && isinf(b) == 1){type = 1; a=0; b=1;}
  if(isinf(a) == 0 && isinf(b) == 1){type = 2;limit = a; a=0; b=1;}
  if(isinf(-a) == 1 && isinf(b) == 0){type = 3; limit = b; a=0; b=1;}
  double f2 = transform(f, a + 2*(b-a)/6, type, limit);
  double f3 = transform(f, a + 4*(b-a)/6, type, limit);
  return adapt24(f,a,b,acc,eps,f2,f3,nrec,type,limit);

}
