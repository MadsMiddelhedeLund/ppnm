#include"ann.h"

double act_fun(double x){
  return exp(-x*x)*x;
}

double test_fun(double x){
  return cos(x)*x;
}

double test_fun_deri(double x){
  return cos(x)-x*sin(x);
}

double test_fun_antideri(double x,double a){
  return x*sin(x)+cos(x)-(a*sin(a)+cos(a));
}

void test(){
  int n = 5; // number of neurons
  ann* network = ann_alloc(n,act_fun);
  double a = -M_PI, b = M_PI; // end points
  int nx = 20; // number of x-points
  gsl_vector* xlist = gsl_vector_alloc(nx);
  gsl_vector* ylist = gsl_vector_alloc(nx);

  // making x and y list
  for(int i = 0; i < nx; i++){
    double x = a + (b-a)*i/(nx-1);
    double y = test_fun(x);
    gsl_vector_set(xlist,i,x);
    gsl_vector_set(ylist,i,y);
  }

  // setting the data to a starting vector
  gsl_vector* data = network -> data;
  for(int i = 0; i < n; i++){
    gsl_vector_set(data,3*i+0,a+(b-a)*i/(n-1));
    gsl_vector_set(data,3*i+1,1);
    gsl_vector_set(data,3*i+2,1);
  }

  FILE* file = fopen("data.dat","w");

  //printing to file
  for(int i=0; i < nx; i++){
    double x = gsl_vector_get(xlist,i);
    double f = gsl_vector_get(ylist,i);
    double df = test_fun_deri(x);
    double F = test_fun_antideri(x,a);
    fprintf(file,"%g %g %g %g\n",x,f,df,F);
  }
  fprintf(file,"\n\n");

  // traning neuraul network
  double dx = 1e-12;
  double eps = 1e-12;
  ann_train(network,xlist,ylist,dx,eps);

  double dz = 1./64;
  for(double z = a; z <= b; z += dz ){
    double y = ann_feed_forward(network,z);
    double dy = ann_feed_forward_deri(network,z);
    double Y = ann_feed_forward_antideri(network,z,a);
    fprintf(file,"%g %g %g %g\n",z,y,dy,Y);
  }

  // Writing text in terminal
  printf("The results is shown ins plot.svg, where the interpolated graph of points to cos(x)*x, the derivative of the interpolated graph and the anti derivative can be see\n");

  // free stuff
  gsl_vector_free(xlist);
  gsl_vector_free(ylist);
  ann_free(network);
}

int main(){
  test();
  return 0;
}
