int binary_search(int n, double* x, double z);

double linterp(int n, double *x, double *y, double z);

double linterp_int(int n, double *x, double* y, double z);

typedef struct {int n; double *x, *y, *b, *c;} qspline;

qspline * qspline_alloc(int n, double* x,double*y);

double qspline_eval(qspline *s, double z);

void qspline_free(qspline *s);

double qspline_int(qspline* s,double z);

double qspline_derivative(qspline* s, double z);

#ifndef HAVE_CSPLINE_H

#define HAVE_CSPLINE_H

typedef struct {int n; double *x,*y,*b,*c,*d;} cubic_spline;

cubic_spline * cubic_spline_alloc (int n, double *x, double *y);

double cubic_spline_eval (cubic_spline * s, double z);

void cubic_spline_free (cubic_spline * s);

#endif

double cubic_spline_int(cubic_spline* s, double z);

double cubic_spline_derivative(cubic_spline* s,double z);
