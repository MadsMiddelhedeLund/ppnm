#include<stdio.h>
#include <stdlib.h>
#include<math.h>
#include"interp.h"
#include <assert.h>

int binary_search(int n, double* x, double z){
  int L = 0;
  int R = n-1;

  do{
    int m=(L+R)/2;
    if(z>x[m]) L = m;
    else R = m;
  }while(R-L>1);

  return L;
}

double linterp(int n, double *x, double *y, double z){

  assert(n>1 && z>=x [0] && z<=x [n-1]); // making sure starting condition is ok
  int i = binary_search(n,x,z);
  double a_i = (y[i+1]-y[i])/(x[i+1]-x[i]);
  double b_i = y[i];

return b_i + a_i*(z-x[i]);
}

double linterp_int(int n, double *x, double* y, double z){
double S_z = linterp(n, x, y, z);

double integral = 0;
int i = 0;
while (x[i+1] < z) {
    integral += (y[i + 1] + y[i])/2*(x[i + 1] - x[i]);
    i++;
}

integral += (S_z + y[i])*(z - x[i])/2;
return integral;
}

qspline * qspline_alloc(int n, double* x,double*y){
  qspline *s = (qspline*) malloc(sizeof(qspline));
  s->b = (double*)malloc((n-1)*sizeof(double)); // dont need for the last one
  s->c = (double*)malloc((n-1)*sizeof(double));
  s->x = (double*)malloc(n*sizeof(double));
  s->y = (double*)malloc(n*sizeof(double));
  s->n = n;

  // builds the x and y into the struct
  for(int i = 0; i < n; i++){
    s->x[i]=x[i];
    s->y[i]=y[i];
  }
  int i;
  double p[n-1], h[n-1];

  for(i = 0;i < n-1; i++){
    h[i] = x[i+1]-x[i];
    p[i] = (y[i+1]-y[i])/h[i];
  }

  s->c[0]=0;

  for(int i=0;i < n-2;i++){
    s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
  }

  s->c[n-2]/=2;

  for(int i = n-3;i>=0;i--){
    s->c[i]=(p[i+1]-p[i]-s->c[i+1]*h[i+1])/h[i];
  }

  for(int i = 0; i < n-1; i++){
    s->b[i]=p[i]-s->c[i]*h[i];
  }

return s;
}

double qspline_eval(qspline *s, double z){
  assert(z>=s->x[0] && z<=s->x[s->n-1]);

  int i = binary_search(s->n,s->x,z);
  double h = z-s->x[i];
  return s->y[i]+h*(s->b[i]+h*s->c[i]);

}

void qspline_free(qspline *s){
  free(s->x);
  free(s->y);
  free(s->b);
  free(s->c);
  free(s);
}

double qspline_int(qspline* s,double z){
  assert(z>=s->x[0] && z<=s->x[s->n-1]);

  double *x = s->x;
  double *a = s->y;
  double *b = s->b;
  double *c = s->c;

  double integral = 0;
  int i = 0;
  while (x[i+1]<z){
    integral +=a[i]*(x[i+1]-x[i])+b[i]*(x[i+1]-x[i])*(x[i+1]-x[i])/2
                +c[i]*(x[i+1]-x[i])*(x[i+1]-x[i])*(x[i+1]-x[i])/3;
    i++;
  }

  integral +=a[i]*(z-x[i])+b[i]*(z-x[i])*(z-x[i])/2
              +c[i]*(z-x[i])*(z-x[i])*(z-x[i])/3;

  return integral;

}

double qspline_derivative(qspline* s, double z){
  assert(z>=s->x[0] && z<=s->x[s->n-1]);

  int n = s->n;
  double *x = s->x;
  double *b = s->b;
  double *c = s->c;

  int i = binary_search(n,x,z);
  return b[i]+2*c[i]*(z-x[i]);
}

cubic_spline *cubic_spline_alloc(int n, double *x, double *y)
{
	cubic_spline *s = (cubic_spline *) malloc(sizeof(cubic_spline));
	s->x = (double *)malloc(n * sizeof(double));
	s->y = (double *)malloc(n * sizeof(double));
	s->b = (double *)malloc(n * sizeof(double));
	s->c = (double *)malloc((n - 1) * sizeof(double));
	s->d = (double *)malloc((n - 1) * sizeof(double));
	s->n = n;

	for (int i = 0; i < n; i++) {
		s->x[i] = x[i];
		s->y[i] = y[i];
	}

	double h[n - 1], p[n - 1];

	for (int i = 0; i < n - 1; i++) {
		h[i] = x[i + 1] - x[i];
		assert(h[i] > 0);
    p[i] = (y[i + 1] - y[i]) / h[i];
	}

	double D[n], Q[n - 1], B[n];
	D[0] = 2;
  Q[0] = 1;

	for (int i = 0; i < n - 2; i++){
		D[i + 1] = 2 * h[i] / h[i + 1] + 2;
    Q[i + 1] = h[i] / h[i + 1];
    B[i + 1] = 3 * (p[i] + p[i + 1] * h[i] / h[i + 1]);
  }

	D[n - 1] = 2;
	B[0] = 3 * p[0];
	B[n - 1] = 3 * p[n - 2];

  // gauss elimination
	for (int i = 1; i < n; i++) {
		D[i] -= Q[i - 1] / D[i - 1];
		B[i] -= B[i - 1] / D[i - 1];
	}

	s->b[n - 1] = B[n - 1] / D[n - 1];

	for (int i = n - 2; i >= 0; i--)
		s->b[i] = (B[i] - Q[i] * s->b[i + 1]) / D[i];

	for (int i = 0; i < n - 1; i++) {
		s->c[i] = (-2 * s->b[i] - s->b[i + 1] + 3 * p[i]) / h[i];
		s->d[i] = (s->b[i] + s->b[i + 1] - 2 * p[i]) / h[i] / h[i];
	}
	return s;
}

double cubic_spline_eval(cubic_spline* s,double z){

  assert(z>=s->x[0] && z<= s->x[s->n-1]);

  int i = binary_search(s->n,s->x,z);
  double h = z-s->x[i];

  return s->y[i]+h*(s->b[i]+h*(s->c[i]+h*s->d[i]));

}

void cubic_spline_free(cubic_spline* s){
  free(s->x);
  free(s->y);
  free(s->b);
  free(s->c);
  free(s->d);
  free(s);
}


double cubic_spline_int(cubic_spline* s,double z){
  assert(z>=s->x[0] && z<=s->x[s->n-1]);

  double *x = s->x;
  double *a = s->y;
  double *b = s->b;
  double *c = s->c;
  double *d = s->d;

  double integral = 0;
  int i = 0;
  while (x[i+1]<z){
    integral +=a[i]*(x[i+1]-x[i])+b[i]*(x[i+1]-x[i])*(x[i+1]-x[i])/2
                +c[i]*(x[i+1]-x[i])*(x[i+1]-x[i])*(x[i+1]-x[i])/3
                +d[i]*pow(x[i+1]-x[i],4)/4;
    i++;
  }

  integral +=a[i]*(z-x[i])+b[i]*(z-x[i])*(z-x[i])/2
              +c[i]*(z-x[i])*(z-x[i])*(z-x[i])/3
              +d[i]*pow(z-x[i],4)/4;;

  return integral;
}

double cubic_spline_derivative(cubic_spline* s,double z){
  
  assert(z>=s->x[0] && z<=s->x[s->n-1]);

  double *x = s->x;
  double *b = s->b;
  double *c = s->c;
  double *d = s->d;
  int n = s->n;
  int i = binary_search(n,x,z);
  double h = z-x[i];
  return b[i]+h*(2*c[i]+3*h*d[i]);
}
