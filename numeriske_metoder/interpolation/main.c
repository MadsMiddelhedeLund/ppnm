#include<stdio.h>
#include <stdlib.h>
#include<math.h>
#include"interp.h"
#include <assert.h>

void linterp_test(){

  // Linear interpolation
  int n = 20; // number of spline points
  double end = 4*M_PI;
  double *x = malloc(sizeof(double)*n);
  double *y_sin = malloc(sizeof(double)*n);
  double *y_cos = malloc(sizeof(double)*n); // integrated

  FILE* math_sin = fopen("math_sin_linterp.data","w");

  for(int i=0; i<n;i++){
    x[i] = end*i/(n-1);
    y_sin[i] = sin(x[i]);
    y_cos[i] = -cos(x[i])+cos(x[0]);
    fprintf(math_sin,"%g %g %g\n",x[i],y_sin[i],y_cos[i]);
  }

  int num_data = 1000;
  double *x_int = malloc(sizeof(double)*num_data);
  double *y_sin_int = malloc(sizeof(double)*num_data);
  double *y_cos_int = malloc(sizeof(double)*num_data);

  FILE* sin_int = fopen("sin_lint.data","w");

  for(int i = 0; i < num_data; i++){
    x_int[i] = end*i/(num_data-1);
    y_sin_int[i] = linterp(n,x,y_sin,x_int[i]);
    y_cos_int[i] = linterp_int(n,x,y_sin,x_int[i]);
    fprintf(sin_int,"%g %g %g\n",x_int[i],y_sin_int[i],y_cos_int[i]);
  }

  free(x);
  free(y_cos);
  free(y_sin);
  free(x_int);
  free(y_cos_int);
  free(y_sin_int);
  fclose(math_sin);
  fclose(sin_int);
}

void qspline_test(){
  int n = 20; // number of spline points
  double end = 4*M_PI;
  double *x = malloc(sizeof(double)*n);
  double *y_sin = malloc(sizeof(double)*n);
  double *y_sin_integrated = malloc(sizeof(double)*n); // integrated
  double *y_sin_derivative = malloc(sizeof(double)*n);
  FILE* math_sin = fopen("math_sin_qspline.data","w");

  for(int i=0; i<n;i++){
    x[i] = end*(i)/(n-1);
    y_sin[i] = sin(x[i]);
    y_sin_integrated[i] = -cos(x[i])+cos(x[0]);
    y_sin_derivative[i] = cos(x[i]);
    fprintf(math_sin,"%g %g %g %g\n",x[i],y_sin[i],y_sin_integrated[i],y_sin_derivative[i]);
  }

  int num_data = 200;
  double *x_int = malloc(sizeof(double)*num_data);
  double *y_sin_int = malloc(sizeof(double)*num_data);
  double *y_cos_int = malloc(sizeof(double)*num_data);
  double *y_sin_deri_int = malloc(sizeof(double)*num_data);

  FILE* sin_int = fopen("sin_qinterp.data","w");

  qspline* s = qspline_alloc(n,x,y_sin);

  for(int i = 0; i<num_data; i++){
    x_int[i] = end*(i)/(num_data-1);
    y_sin_int[i] = qspline_eval(s,x_int[i]);
    y_cos_int[i] = qspline_int(s,x_int[i]);
    y_sin_deri_int[i] = qspline_derivative(s,x_int[i]);
    fprintf(sin_int,"%g %g %g %g\n",x_int[i],y_sin_int[i],y_cos_int[i],y_sin_deri_int[i]);
  }
  qspline_free(s);
  free(x);
  free(y_sin);
  free(y_sin_integrated);
  free(y_sin_derivative);
  free(x_int);
  free(y_cos_int);
  free(y_sin_int);
  free(y_sin_deri_int);
  fclose(math_sin);
  fclose(sin_int);
}

void cspline_test(){
  int n = 20; // number of spline points
  double end = 4*M_PI;
  double *x = malloc(sizeof(double)*n);
  double *y_sin = malloc(sizeof(double)*n);
  double *y_sin_integrated = malloc(sizeof(double)*n); // integrated
  double *y_sin_derivative = malloc(sizeof(double)*n);
  FILE* math_sin = fopen("math_sin_cspline.data","w");

  for(int i=0; i<n;i++){
    x[i] = end*(i)/(n-1);
    y_sin[i] = sin(x[i]);
    y_sin_integrated[i] = -cos(x[i])+cos(x[0]);
    y_sin_derivative[i] = cos(x[i]);
    fprintf(math_sin,"%g %g %g %g\n",x[i],y_sin[i],y_sin_integrated[i],y_sin_derivative[i]);
  }

  int num_data = 200;
  double *x_int = malloc(sizeof(double)*num_data);
  double *y_sin_int = malloc(sizeof(double)*num_data);
  double *y_cos_int = malloc(sizeof(double)*num_data);
  double *y_sin_deri_int = malloc(sizeof(double)*num_data);

  FILE* sin_int = fopen("sin_cspline.data","w");

  cubic_spline* s = cubic_spline_alloc(n,x,y_sin);

  for(int i = 0; i<num_data; i++){
    x_int[i] = end*(i)/(num_data-1);
    y_sin_int[i] = cubic_spline_eval(s,x_int[i]);
    y_cos_int[i] = cubic_spline_int(s,x_int[i]);
    y_sin_deri_int[i] = cubic_spline_derivative(s,x_int[i]);
    fprintf(sin_int,"%g %g %g %g\n",x_int[i],y_sin_int[i], y_cos_int[i],y_sin_deri_int[i]);
  }
  cubic_spline_free(s);
  free(x);
  free(y_sin);
  free(y_sin_integrated);
  free(y_sin_derivative);
  free(x_int);
  free(y_sin_int);
  free(y_cos_int);
  free(y_sin_deri_int);
  fclose(math_sin);
  fclose(sin_int);

}

int main(){
  linterp_test();
  qspline_test();
  cspline_test();
return 0;
}
