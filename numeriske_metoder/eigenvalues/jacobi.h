int jacobi(gsl_matrix*A, gsl_vector* e, gsl_matrix* V);

int jacobi_eig_by_eig(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int N, double theta);
