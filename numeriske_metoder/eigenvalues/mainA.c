#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"jacobi.h"
#include<assert.h>
#define RND ((double)rand()/RAND_MAX)

void rnd_sym_matrix(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  assert(n==m);

  for(int i = 0; i < n; i++){
    for(int j = i; j < n;j++){
      gsl_matrix_set(A,i,j,RND);
      gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
    }
  }
}

void print_matrix(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  for(int i = 0; i < n;i++){
    for(int j = 0; j < m; j++){
      printf("%7.3f\t",gsl_matrix_get(A,i,j));
    }
    printf("\n");
  }
  printf("\n");
}

void print_vector(gsl_vector* v){
  int n = v->size;
  for(int i = 0; i < n; i++){
    printf("% 7.3f\n",gsl_vector_get(v,i));
  }
  printf("\n");
}

void test_jacobi(int n){
  printf("-------Exercise A--------\n");
  gsl_matrix* A = gsl_matrix_alloc(n,n);
  gsl_matrix* A_copy = gsl_matrix_alloc(n,n);
  gsl_matrix* V = gsl_matrix_alloc(n,n);
  gsl_vector* e = gsl_vector_alloc(n);

  rnd_sym_matrix(A);
  gsl_matrix_memcpy(A_copy,A);

  printf("A=\n");
  print_matrix(A);

  int sweeps = jacobi(A,e,V);
  
  printf("\nnumber of sweeps = %i\n",sweeps);

  printf("eigenvalues=\n");
  print_vector(e);

  printf("eigenvectors V=(v_1 v_2 .... v_n)\n");
  print_matrix(V);


  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_copy,V,0.0,A);
  gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A,0.0,A_copy);
  printf("V^T*A*V=\n");
  print_matrix(A_copy);


  gsl_matrix_free(A);
  gsl_matrix_free(V);
  gsl_vector_free(e);
  gsl_matrix_free(A_copy);
}


int main(int argc, char** argv){
  int n=(argc>1? atoi(argv[1]):5);
  test_jacobi(n);
return 0;
}
