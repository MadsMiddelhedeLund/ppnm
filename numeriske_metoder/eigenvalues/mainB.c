#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"jacobi.h"
#include<assert.h>
#define RND ((double)rand()/RAND_MAX)

void rnd_sym_matrix(gsl_matrix* A){
  int n = A->size1;
  int m = A->size2;
  assert(n==m);

  for(int i = 0; i < n; i++){
    for(int j = i; j < n;j++){
      gsl_matrix_set(A,i,j,RND);
      gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
    }
  }
}

void print_matrix(gsl_matrix* A,int N){
  int n = A->size1;
  //int m = A->size2;
  for(int i = 0; i < n;i++){
    for(int j = 0; j < N; j++){
      printf("%7.3f\t",gsl_matrix_get(A,i,j));
    }
    printf("\n");
  }
  printf("\n");
}

void print_vector(gsl_vector* v,int N){
  //int n = v->size;
  for(int i = 0; i < N; i++){
    printf("% 7.3f\n",gsl_vector_get(v,i));
  }
  printf("\n");
}

void test_jacobi_eig_by_eig(int n, int N){
  printf("\n\n-------Exercise B--------\n");
  gsl_matrix* A = gsl_matrix_alloc(n,n);
  gsl_matrix* A_copy1 = gsl_matrix_alloc(n,n);
  gsl_matrix* A_copy2 = gsl_matrix_alloc(n,n);
  gsl_matrix* V = gsl_matrix_alloc(n,n);
  gsl_vector* e = gsl_vector_alloc(n);

  rnd_sym_matrix(A);
  gsl_matrix_memcpy(A_copy1,A);
  gsl_matrix_memcpy(A_copy2,A);
  printf("A=\n");
  print_matrix(A,n);

  int sweeps_lowest = jacobi_eig_by_eig(A,e,V,N,0);

  printf("\nnumber of sweeps = %i\n",sweeps_lowest);

  printf("%i lowest eigen values:\n",N);
  print_vector(e,N);

  printf("eigenvectors V to smallest to largest eigenvalues=(v_1 v_2 .... v_N)\n");
  print_matrix(V,N);

  int sweeps_highest = jacobi_eig_by_eig(A_copy2,e,V,N,M_PI/2);

  printf("\nnumber of sweeps = %i\n",sweeps_highest);

  printf("%i largest eigen values:\n",N);
  print_vector(e,N);

  printf("eigenvectors to largest to smallest eigenvalues V=(v_1 v_2 .... v_N)\n");
  print_matrix(V,N);

  gsl_matrix_free(A);
  gsl_matrix_free(V);
  gsl_vector_free(e);
  gsl_matrix_free(A_copy1);
  gsl_matrix_free(A_copy2);
}

int main(int argc, char** argv){
  int n=(argc>1? atoi(argv[1]):5);
  int N=(argc>1? atoi(argv[2]):2);
  assert(n>=N);
  test_jacobi_eig_by_eig(n,N);
return 0;
}
