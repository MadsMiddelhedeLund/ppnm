#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<assert.h>

typedef struct {double (*function)(double, int); int m;} fit_function;

void qr_gs_decomp(gsl_matrix *A, gsl_matrix *R);

void lssq_fit(const fit_function* fun, const gsl_vector* x, const gsl_vector* y, const gsl_vector* dy, gsl_vector* c, gsl_matrix* S);

void qr_gs_solve(const gsl_matrix *Q, const gsl_matrix *R, const gsl_vector *b, gsl_vector *x);

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix *R, gsl_matrix *B);
