#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_matrix.h>
#include"fit.h"


void data1(gsl_vector* x, gsl_vector* y,gsl_vector* dy){

    gsl_vector_set(x, 0, 0.1);
    gsl_vector_set(x, 1, 1.33);
    gsl_vector_set(x, 2, 2.55);
    gsl_vector_set(x, 3, 3.78);
    gsl_vector_set(x, 4, 5.);
    gsl_vector_set(x, 5, 6.22);
    gsl_vector_set(x, 6, 7.45);
    gsl_vector_set(x, 7, 8.68);
    gsl_vector_set(x, 8, 9.9);

    gsl_vector_set(y, 0, -15.3);
    gsl_vector_set(y, 1, 0.32);
    gsl_vector_set(y, 2, 2.45);
    gsl_vector_set(y, 3, 2.75);
    gsl_vector_set(y, 4, 2.27);
    gsl_vector_set(y, 5, 1.35);
    gsl_vector_set(y, 6, 0.157);
    gsl_vector_set(y, 7, -1.23);
    gsl_vector_set(y, 8, -2.75);

    gsl_vector_set(dy, 0, 1.04);
    gsl_vector_set(dy, 1, 0.594);
    gsl_vector_set(dy, 2, 0.983);
    gsl_vector_set(dy, 3, 0.998);
    gsl_vector_set(dy, 4, 1.11);
    gsl_vector_set(dy, 5, 0.398);
    gsl_vector_set(dy, 6, 0.535);
    gsl_vector_set(dy, 7, 0.968);
    gsl_vector_set(dy, 8, 0.478);
}


double fit_fun1(double x, int i){
  switch(i){
    case 0: return log(x); break;
    case 1: return 1.0;   break;
    case 2: return x;     break;
    default: {fprintf(stderr,"Error in fitting_functions: wrong i:%d",i); return NAN;}
  }
}

void test_fit1(){
  int n = 9; // number of points
  gsl_vector* x = gsl_vector_alloc(n);
  gsl_vector* y = gsl_vector_alloc(n);
  gsl_vector* dy = gsl_vector_alloc(n);

  data1(x,y,dy);

  int m = 3; // number of functions

  fit_function fun;
  fun.function = fit_fun1;
  fun.m = m;

  gsl_vector* c = gsl_vector_alloc(m);
  gsl_matrix* S = gsl_matrix_alloc(m,m);

  lssq_fit(&fun,x,y,dy,c,S);

  FILE* file = fopen("fit1.dat","w");

  for(int i = 0; i < n; i++){
    fprintf(file,"%g %g %g\n",gsl_vector_get(x,i),gsl_vector_get(y,i),gsl_vector_get(dy,i));
  }

  fprintf(file,"\n\n");
  // now i'm making the plot data

  double x_min = gsl_vector_get(x,0);
  double x_max = gsl_vector_get(x,n-1);
  double dx = (x_max-x_min)/1000;

  for (double x_plot=x_min; x_plot < x_max; x_plot += dx){
    double f = 0;
    double f_minus = 0;
    double f_plus = 0;
    for (int i = 0; i < m; i++){
      double c_i = gsl_vector_get(c,i);
      double dc_i = gsl_matrix_get(S,i,i);
      f += c_i*fit_fun1(x_plot,i);
      f_minus += (c_i-dc_i)*fit_fun1(x_plot,i);
      f_plus += (c_i+dc_i)*fit_fun1(x_plot,i);
    }
    fprintf(file,"%g %g %g %g\n",x_plot,f,f_minus,f_plus);
  }

  fclose(file);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_vector_free(dy);
  gsl_vector_free(c);
  gsl_matrix_free(S);
}

void data2(gsl_vector* x, gsl_vector* y, gsl_vector* dy){

    gsl_vector_set(x, 0, 0.1);
    gsl_vector_set(x, 1, 0.145);
    gsl_vector_set(x, 2, 0.211);
    gsl_vector_set(x, 3, 0.307);
    gsl_vector_set(x, 4, 0.447);
    gsl_vector_set(x, 5, 0.649);
    gsl_vector_set(x, 6, 0.944);
    gsl_vector_set(x, 7, 1.372);
    gsl_vector_set(x, 8, 1.995);
    gsl_vector_set(x, 9, 2.900);

    gsl_vector_set(y, 0, 12.644);
    gsl_vector_set(y, 1, 9.235);
    gsl_vector_set(y, 2, 7.377);
    gsl_vector_set(y, 3, 6.460);
    gsl_vector_set(y, 4, 5.555);
    gsl_vector_set(y, 5, 5.896);
    gsl_vector_set(y, 6, 5.673);
    gsl_vector_set(y, 7, 6.964);
    gsl_vector_set(y, 8, 8.896);
    gsl_vector_set(y, 9, 11.355);

    gsl_vector_set(dy, 0, 0.858);
    gsl_vector_set(dy, 1, 0.359);
    gsl_vector_set(dy, 2, 0.505);
    gsl_vector_set(dy, 3, 0.403);
    gsl_vector_set(dy, 4, 0.683);
    gsl_vector_set(dy, 5, 0.605);
    gsl_vector_set(dy, 6, 0.856);
    gsl_vector_set(dy, 7, 0.351);
    gsl_vector_set(dy, 8, 1.083);
    gsl_vector_set(dy, 9, 1.002);

}

double fit_fun2(double x, int i){
  switch(i){
    case 0: return 1/x; break;
    case 1: return 1.0; break;
    case 2: return x; break;
    default: {fprintf(stderr,"Error in fit_fun2: wrong i: %d",i); return NAN;}
  }
}

void test_fit2(){
  int n = 10; // number of points
  gsl_vector* x = gsl_vector_alloc(n);
  gsl_vector* y = gsl_vector_alloc(n);
  gsl_vector* dy = gsl_vector_alloc(n);

  data2(x,y,dy);

  int m = 3; // number of functions

  fit_function fun;
  fun.function = fit_fun2;
  fun.m = m;

  gsl_vector* c = gsl_vector_alloc(m);
  gsl_matrix* S = gsl_matrix_alloc(m,m);

  lssq_fit(&fun,x,y,dy,c,S);

  FILE* file = fopen("fit2.dat","w");

  for(int i = 0; i < n; i++){
    fprintf(file,"%g %g %g\n",gsl_vector_get(x,i),gsl_vector_get(y,i),gsl_vector_get(dy,i));
  }

  fprintf(file,"\n\n");
  // now i'm making the plot data

  double x_min = gsl_vector_get(x,0);
  double x_max = gsl_vector_get(x,n-1);
  double dx = (x_max-x_min)/1000;

  for (double x_plot=x_min; x_plot < x_max; x_plot += dx){
    double f = 0;
    double f_minus = 0;
    double f_plus = 0;
    for (int i = 0; i < m; i++){
      double c_i = gsl_vector_get(c,i);
      double dc_i = gsl_matrix_get(S,i,i);
      f += c_i*fit_fun2(x_plot,i);
      f_minus += (c_i-dc_i)*fit_fun2(x_plot,i);
      f_plus += (c_i+dc_i)*fit_fun2(x_plot,i);
    }
    fprintf(file,"%g %g %g %g\n",x_plot,f,f_minus,f_plus);
  }

  fclose(file);
  gsl_vector_free(x);
  gsl_vector_free(y);
  gsl_vector_free(dy);
  gsl_vector_free(c);
  gsl_matrix_free(S);
}
int main(){
  test_fit1();
  test_fit2();
return 0;
}
