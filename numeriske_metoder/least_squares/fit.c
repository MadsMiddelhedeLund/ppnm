#include"fit.h"

void lssq_fit_init(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, const fit_function* fun,const gsl_vector* x, const gsl_vector* y, const gsl_vector* dy){

  // dimension of the matrices
  int n = x->size; // number of data
  int m  = fun->m; // number of functions

  // making b
  for(int i = 0; i < n; i++){
    double y_i = gsl_vector_get(y,i);
    double dy_i = gsl_vector_get(dy,i);
    double b_i = y_i/dy_i;
    gsl_vector_set(b,i,b_i);
  }

  // making A/Q
  for(int i = 0; i < n; i++){
    for(int k = 0; k < m; k++){
      double x_i = gsl_vector_get(x,i);
      double dy_i = gsl_vector_get(dy,i);
      double f_ki = (fun->function)(x_i,k);
      f_ki = f_ki/dy_i;
      gsl_matrix_set(Q,i,k,f_ki);
    }
  }

  qr_gs_decomp(Q,R);
}

void lssq_fit(const fit_function* fun, const gsl_vector* x, const gsl_vector* y, const gsl_vector* dy, gsl_vector* c, gsl_matrix* S){

  int n = x->size; // number of data
  int m  = fun->m;

  gsl_matrix* Q = gsl_matrix_alloc(n,m);
  gsl_matrix* R = gsl_matrix_alloc(m,m);
  gsl_matrix* invR = gsl_matrix_alloc(m,m);
  gsl_vector* b = gsl_vector_alloc(n);
  gsl_matrix* I = gsl_matrix_alloc(m,m);

  lssq_fit_init(Q,R,b,fun,x,y,dy);

  qr_gs_solve(Q,R,b,c);

  // Finding Covariance S

  gsl_matrix_set_identity(I);
  qr_gs_inverse(I,R,invR);
  gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,invR,invR,0,S);



  gsl_vector_free(b);
  gsl_matrix_free(Q);
  gsl_matrix_free(R);
  gsl_matrix_free(invR);
  gsl_matrix_free(I);


}
