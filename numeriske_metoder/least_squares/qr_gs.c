#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"fit.h"

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
  int m = A->size2;

  for(int i = 0; i < m; i++){
    gsl_vector_view ai = gsl_matrix_column(A,i);
    double norm = gsl_blas_dnrm2(&ai.vector);
    gsl_matrix_set(R,i,i,norm);
    gsl_vector_scale(&ai.vector,1/norm);
    for(int j = i+1; j<m;j++){
      gsl_vector_view aj = gsl_matrix_column(A,j);
      double dotprod;
      gsl_blas_ddot(&ai.vector,&aj.vector,&dotprod);
      gsl_blas_daxpy(-dotprod,&ai.vector,&aj.vector);
      gsl_matrix_set(R,i,j,dotprod);
      gsl_matrix_set(R,j,i,0);
    }
  }
}

void qr_gs_solve(const gsl_matrix* Q,const gsl_matrix* R,const gsl_vector* b,gsl_vector* x) {
  int m = R->size1;

  gsl_blas_dgemv(CblasTrans,1.0,Q,b,0,x);

  // back substitution
  for(int i = m-1;i>=0;i--){
    double s = gsl_vector_get(x,i);
    for(int k=i+1;k < m; k++){
      s-=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
    }
    s /= gsl_matrix_get(R,i,i);
    gsl_vector_set(x,i,s);
  }
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
  int n = R->size1;
  gsl_matrix_set_identity(B);

  gsl_vector_view v;
  gsl_vector* x= gsl_vector_alloc(n);
  for(int i = 0; i < n; i++){
    v = gsl_matrix_column(B,i);
    qr_gs_solve(Q,R,&v.vector,x);
    gsl_matrix_set_col(B,i,x);
  }

}
