#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
//#define EPS (1.0/524288)
#define EPS (1.0/1048576)
//#define EPS (1.0/2097152)
//#define EPS 1.5e-8
int newton(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double eps);

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);

void qr_gs_solve(const gsl_matrix* Q,const gsl_matrix* R,const gsl_vector* b,gsl_vector* x);

int qnewton_broyden_num(double f(gsl_vector* x),gsl_vector* x, double dx, double eps);

void numeric_gradient(double f(gsl_vector*), gsl_vector*x, gsl_vector*grad_f,double dx);

void broyden_update(double f(gsl_vector* x), gsl_matrix* B, gsl_vector* z,gsl_vector* x, gsl_vector* gz, gsl_vector* df, gsl_vector* y, gsl_vector* Dx, gsl_vector* u, double lambda, double lambda_eps, double dx);

void print_vector(gsl_vector* x);
