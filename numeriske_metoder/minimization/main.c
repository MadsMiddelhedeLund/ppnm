#include"minimization.h"


double rosenbrock_gh(gsl_vector* x, gsl_vector* df, gsl_matrix* H){
  double X = gsl_vector_get(x,0);
  double Y = gsl_vector_get(x,1);
  gsl_vector_set(df,0,-2*(1-X)-400*(Y-X*X)*X);
  gsl_vector_set(df,1,200.0*(Y-X*X));
  gsl_matrix_set(H,0,0,2.0-400.0*(Y-3*X*X));
  gsl_matrix_set(H,0,1,-400.0*X);
  gsl_matrix_set(H,1,0,-400.0*X);
  gsl_matrix_set(H,1,1,200.0);
  return (1-X)*(1-X)+100*(Y-X*X)*(Y-X*X);
}

double himmelblau_gh(gsl_vector* x, gsl_vector* df, gsl_matrix* H){
  double X = gsl_vector_get(x,0);
  double Y = gsl_vector_get(x,1);
  gsl_vector_set(df,0,2*(2*X*(X*X+Y-11)+X+Y*Y-7));
  gsl_vector_set(df,1,2*(X*X+2*Y*(X+Y*Y-7)+Y-11));
  gsl_matrix_set(H,0,0,4*(X*X+Y-11)+8*X*X+2);
  gsl_matrix_set(H,0,1,4*X+4*Y);
  gsl_matrix_set(H,1,0,4*X+4*Y);
  gsl_matrix_set(H,1,1,4*(X+Y*Y-7)+8*Y*Y+2);
  return (X*X+Y-11)*(X*X+Y-11)+(X+Y*Y-7)*(X+Y*Y-7);
}

double rosenbrock(gsl_vector* x){
  double X = gsl_vector_get(x,0);
  double Y = gsl_vector_get(x,1);
  return (1-X)*(1-X)+100*(Y-X*X)*(Y-X*X);;
}

double himmelblau(gsl_vector* x){
  double X = gsl_vector_get(x,0);
  double Y = gsl_vector_get(x,1);
  return (X*X+Y-11)*(X*X+Y-11)+(X+Y*Y-7)*(X+Y*Y-7);
}

double fit_fun(gsl_vector* params){
  double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
  double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
  double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
  int N = sizeof(t)/sizeof(t[0]);
  double A = gsl_vector_get(params,0);
  double T = gsl_vector_get(params,1);
  double B = gsl_vector_get(params,2);

  double residual=0;

  for(int i = 0; i < N; i++){
    residual += (A*exp(-t[i]/T)+B-y[i])*(A*exp(-t[i]/T)+B-y[i])/(e[i]*e[i]);
  }
  return residual;
}

void test_a(){

  double eps = 1e-6;
  int iter;

  gsl_vector* x = gsl_vector_alloc(2);
  double y;
  gsl_vector* df = gsl_vector_alloc(2);
  gsl_matrix* H = gsl_matrix_alloc(2,2);

  printf("------------- Part A -------------\n\n");

  printf("Minimization of Rosenbrock function using analytical gradient\n\n ");
  gsl_vector_set(x,0,0);
  gsl_vector_set(x,1,0);
  printf("Starting values for x=\n");
  print_vector(x);
  iter = newton(rosenbrock_gh,x,eps);
  printf("The minimum was found to be x_min =\n");
  print_vector(x);
  printf("The minimum was found in %i steps\n",iter);
  y = rosenbrock_gh(x,df,H);
  printf("gradient(f(x_min)) = \n");
  print_vector(df);
  printf("f(x_min)=%10.4g\n",y);

  printf("\nMinimization of himmelblau function using analytical gradient\n\n ");
  gsl_vector_set(x,0,3);
  gsl_vector_set(x,1,-3);
  printf("Starting values for x=\n");
  print_vector(x);
  iter = newton(himmelblau_gh,x,eps);
  printf("The minimum was found to be x_min =\n");
  print_vector(x);
  printf("The minimum was found in %i steps\n",iter);
  y = himmelblau_gh(x,df,H);
  printf("gradient(f(x_min)) = \n");
  print_vector(df);
  printf("f(x_min)=%10.4g\n",y);

  gsl_vector_free(x);
  gsl_vector_free(df);
  gsl_matrix_free(H);
}

void test_b(){
  double eps = 1e-6, dx = 1e-12;
  int iter;

  gsl_vector* x = gsl_vector_alloc(2);
  gsl_vector* df = gsl_vector_alloc(2);
  double y;

  printf("\n\n------------- Part B -------------\n\n");

  printf("\nMinimization of Rosenbrock function using broyden update and numerical calculated gradient\n\n");
  gsl_vector_set(x,0,0);
  gsl_vector_set(x,1,0);
  printf("Starting values for x=\n");
  print_vector(x);
  iter = qnewton_broyden_num(rosenbrock,x,dx,eps);
  printf("The minimum was found to be x_min =\n");
  print_vector(x);
  printf("The minimum was found in %i steps\n",iter);
  y = rosenbrock(x);
  numeric_gradient(rosenbrock,x,df,dx);
  printf("gradient(f(x_min)) = \n");
  print_vector(df);
  printf("f(x_min)=%10.4g\n",y);

  printf("\nMinimization of himmelblau function using using broyden update and numerical calculated gradient\n\n");
  gsl_vector_set(x,0,3);
  gsl_vector_set(x,1,-3);
  printf("Starting values for x=\n");
  print_vector(x);
  iter = qnewton_broyden_num(himmelblau,x,dx,eps);
  printf("The minimum was found to be x_min =\n");
  print_vector(x);
  printf("The minimum was found in %i steps\n",iter);
  y = himmelblau(x);
  numeric_gradient(himmelblau,x,df,dx);
  printf("gradient(f(x_min)) = \n");
  print_vector(df);
  printf("f(x_min)=%10.4g\n",y);

  printf("\n\nFitting to data \n");
  dx = 1e-6; // 1e-12 is to large
  gsl_vector* params = gsl_vector_alloc(3);
  gsl_vector_set(params,0,1);
  gsl_vector_set(params,1,1);
  gsl_vector_set(params,2,1);

  iter = qnewton_broyden_num(fit_fun,params,dx,eps);

  printf("The solution is found to be [A;T;B] = \n");
  print_vector(params);
  printf("Converging in %i steps.\n\n",iter);
  numeric_gradient(fit_fun,params,df,dx);
  printf("Gradient(f(x_min))=\n");
  print_vector(df);

  FILE* file = fopen("data.txt", "w");

  double t_dat[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
  double y_dat[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
  double e_dat[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
  int N = sizeof(t_dat)/sizeof(t_dat[0]);

  for (int i = 0; i < N; ++i) {
      fprintf(file, "%g %g %g\n", t_dat[i], y_dat[i], e_dat[i]);
  }
  fprintf(file, "\n\n");
  double A = gsl_vector_get(params, 0);
  double T = gsl_vector_get(params, 1);
  double B = gsl_vector_get(params, 2);
  for (double x = t_dat[0]; x < t_dat[N-1]+1e-5; x+=0.1) {
      fprintf(file, "%g %g\n", x, A*exp(-x/T) + B);
  }
  fclose(file);

  // free stuff
  gsl_vector_free(x);
  gsl_vector_free(params);
}


int main(){
  test_a();
  test_b();
  return 0;
}
