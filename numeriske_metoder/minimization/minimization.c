#include"minimization.h"

void print_vector(gsl_vector* x){
  int n = x->size;
  for(int i = 0; i < n; i ++){
    printf("%10.4g \n",gsl_vector_get(x,i));
  }
}

int newton(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double eps){
  int n = x->size;
  int iter = 0;
  int iter_max = 1e6;
  double fx, lambda, alpha, norm_df, DxTdf;

  gsl_matrix* H = gsl_matrix_alloc(n,n);
  gsl_matrix* R = gsl_matrix_alloc(n,n);
  gsl_vector* df = gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n);
  gsl_vector* z  = gsl_vector_alloc(n);
  do {
    fx = f(x,df,H);
		// solve for DX
		qr_gs_decomp(H,R);
		qr_gs_solve(H,R,df,Dx);
		gsl_vector_scale(Dx,-1);

    lambda = 1.0;
    alpha = 1e-2;

    DxTdf = gsl_blas_ddot(Dx,df,&DxTdf);
    while (1) {
            gsl_vector_memcpy(z,x);
            gsl_vector_add(z, Dx); //x = x - lambda*Delta_x
            if(f(z,df,H) < (fx + alpha*lambda*DxTdf) || lambda < 1.0/64.0) break;
            lambda *=0.5;
            gsl_vector_scale(Dx,0.5);
        }
        gsl_vector_memcpy(x,z);
        fx = f(x,df,H);
        norm_df = gsl_blas_dnrm2(df);
        iter++;

  }while(norm_df > eps && iter < iter_max);


  gsl_matrix_free(H);
  gsl_matrix_free(R);
  gsl_vector_free(df);
  gsl_vector_free(Dx);
  gsl_vector_free(z);

  return iter;
}


void numeric_gradient(double f(gsl_vector*), gsl_vector*x, gsl_vector*grad_f,double dx){
  double fx = f(x);
   for (int i = 0; i < grad_f->size; i++) {
       double x_i = gsl_vector_get(x, i);
       gsl_vector_set(x, i, x_i + dx);
       double fx_dx = f(x);
       gsl_vector_set(x, i, x_i);
       double grad_f_i = (fx_dx - fx) / dx;
       gsl_vector_set(grad_f, i, grad_f_i);
	}
}

void broyden_update(double f(gsl_vector* x), gsl_matrix* B, gsl_vector* z,gsl_vector* x, gsl_vector* gz, gsl_vector* df, gsl_vector* y, gsl_vector* Dx, gsl_vector* u, double lambda, double lambda_eps, double dx){
  if(lambda > lambda_eps){
    numeric_gradient(f,z,gz,dx);
    gsl_vector_memcpy(y,gz);
    gsl_blas_daxpy(-1,df,y); /* y=grad(z)-grad(x) */
    gsl_vector_memcpy(u,Dx); /* u=s */
    gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); /* u=s-By */
    double sTy,uTy;
    gsl_blas_ddot(Dx,y,&sTy);
    if(fabs(sTy)>1e-12){
      gsl_blas_ddot(u,y,&uTy);
      double gamma=uTy/2/sTy;
      gsl_blas_daxpy(-gamma,Dx,u); /* u=u-gamma*s */
      gsl_blas_dger(1.0/sTy,u,Dx,B);
      gsl_blas_dger(1.0/sTy,Dx,u,B);
    }
  }
  else{
    gsl_matrix_set_identity(B);
  }
}
int qnewton_broyden_num(double f(gsl_vector* x),gsl_vector* x, double dx, double eps){
  int n = x-> size;
  int iter = 0;
  int iter_max = 1e3;
  double lambda_eps = 1e-6;


  double lambda, norm_df, fx, alpha,DxTdf;

  gsl_matrix* B = gsl_matrix_alloc(n,n);
  gsl_vector* df = gsl_vector_alloc(n);
  gsl_vector* u = gsl_vector_alloc(n);
  gsl_vector* y = gsl_vector_alloc(n);
  gsl_vector* Dx = gsl_vector_alloc(n);
  gsl_vector* z = gsl_vector_alloc(n);
  gsl_vector* gz = gsl_vector_alloc(n);
  gsl_matrix_set_identity(B);
  numeric_gradient(f,x,df,dx);
  fx = f(x);
  while(1){
    iter ++;

    // finding Dx
    gsl_blas_dgemv(CblasNoTrans,-1,B,df,0,Dx);

    lambda = 1;
    alpha = 1e-2;

    DxTdf = gsl_blas_ddot(Dx,df,&DxTdf);
    // back tracking
    while (1) {
            gsl_vector_memcpy(z,x);
            gsl_vector_add(z, Dx); //x = x - lambda*Delta_x
            if(f(z) < (fx + alpha*lambda*DxTdf) || lambda < lambda_eps) break;
            lambda *=0.5;
            gsl_vector_scale(Dx,0.5);
        }
    norm_df = gsl_blas_dnrm2(df);

    if (norm_df < eps || iter > iter_max) {break;}
    else{
      broyden_update(f,B,z,x,gz,df,y,Dx,u,lambda,lambda_eps,dx);
      gsl_vector_memcpy(x,z);
      gsl_vector_memcpy(df,gz);
      fx=f(x);
    }
  }

  //free stuff
  gsl_matrix_free(B);
  gsl_vector_free(df);
  gsl_vector_free(u);
  gsl_vector_free(y);
  gsl_vector_free(Dx);
  gsl_vector_free(z);
  gsl_vector_free(gz);

  return iter;
}
