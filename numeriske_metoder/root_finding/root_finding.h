#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multiroots.h>

int newton_with_jacobian(void f(gsl_vector* x,gsl_vector* fx, gsl_matrix* J ),gsl_vector* x, double eps);

int newton_jacobian_num(void f(gsl_vector* x,gsl_vector* fx),gsl_vector* x, double dx, double eps);

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);

void qr_gs_solve(const gsl_matrix* Q,const gsl_matrix* R,const gsl_vector* b,gsl_vector* x);

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B);
