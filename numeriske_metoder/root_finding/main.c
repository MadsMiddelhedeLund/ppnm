#include"root_finding.h"
int ncalls;
void print_vector(gsl_vector* x){
  int n = x->size;
  for(int i = 0; i < n; i ++){
    printf("%10.4g \n",gsl_vector_get(x,i));
  }
}

void rosenbrock_jac(gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
    double X = gsl_vector_get(x,0);
    double Y = gsl_vector_get(x,1);
    gsl_vector_set(fx,0,2.0*(X-1)-400.0*X*(Y-X*X));
    gsl_vector_set(fx,1,200.0*(Y-X*X));
    gsl_matrix_set(J, 0, 0, 2.0*(600.0*X*X - 200.0*Y + 1.0));
    gsl_matrix_set(J, 0, 1, -400.0*X);
    gsl_matrix_set(J, 1, 0, -400.0*X);
    gsl_matrix_set(J, 1, 1, 200.0);
    ncalls++;
}

void f_jac(gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
	double A = 10000;
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,1,A*Y*X-1.0);
	gsl_vector_set(fx,1,exp(-X)+exp(-Y)-1.0-1.0/A);
	gsl_matrix_set(J,0,0,A*Y);
	gsl_matrix_set(J,0,1,A*X);
	gsl_matrix_set(J,1,0,-1.0*exp(-X));
	gsl_matrix_set(J,1,1,-1.0*exp(-Y));
  ncalls++;
}

void himmelblau_jac(gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
        double X = gsl_vector_get(x,0);
        double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx, 0, 2.0*(2.0*X*(X*X + Y -11.0) + X + Y*Y - 7.0));
	gsl_vector_set(fx, 1, 2.0*(X*X + 2.0*Y*(X + Y*Y - 7.0) + Y - 11.0));

	gsl_matrix_set(J, 0, 0, 2*(4.0*X*X + 2.0*(X*X +Y -11.0) +1.0));
	gsl_matrix_set(J, 0, 1, 2.0*(2.0*X+2.0*Y));
	gsl_matrix_set(J, 1, 0, 2.0*(2.0*X+2.0*Y));
	gsl_matrix_set(J, 1, 1, 2.0*(4.0*Y*Y + 2.0*(Y*Y + X - 7.0) +1.0));
  ncalls++;
}

void f(gsl_vector* x, gsl_vector* fx){
	double A=10000;
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,0,A*Y*X-1.0);
	gsl_vector_set(fx,1,exp(-X)+exp(-Y)-1.0-1.0/A);
  ncalls++;
}

void rosenbrock(gsl_vector* x, gsl_vector* fx){
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,0,2*(X-1)-400*X*(Y-X*X));
	gsl_vector_set(fx,1,200*(Y-X*X));
  ncalls++;
}

void himmelblau(gsl_vector* x, gsl_vector* fx){
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx, 0, 2.0*(2.0*X*(X*X + Y -11.0) + X + Y*Y - 7.0));
	gsl_vector_set(fx, 1, 2.0*(X*X + 2.0*Y*(X + Y*Y - 7.0) + Y - 11.0));
  ncalls++;
}

int himmelblau_gsl(const gsl_vector* x, void* params, gsl_vector* fx){
  double X = gsl_vector_get(x,0);
  double Y = gsl_vector_get(x,1);
  gsl_vector_set(fx, 0, 2.0*(2.0*X*(X*X + Y -11.0) + X + Y*Y - 7.0));
	gsl_vector_set(fx, 1, 2.0*(X*X + 2.0*Y*(X + Y*Y - 7.0) + Y - 11.0));
	return GSL_SUCCESS;
}

int rosenbrock_gsl(const gsl_vector* x, void* params, gsl_vector* fx){
  double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,0,2*(X-1)-400*X*(Y-X*X));
	gsl_vector_set(fx,1,200*(Y-X*X));
	return GSL_SUCCESS;
}

void test_a(){
  gsl_vector* x = gsl_vector_alloc(2);
  double eps = 1e-6;
  int iter;

  printf("\nTesting rooting finding with jacobian given by user\n\n");

  // system of equuations given by dmitri
  ncalls = 0;
  gsl_vector_set(x,0,0);
  gsl_vector_set(x,1,1);
  iter = newton_with_jacobian(f_jac,x,eps);
  printf("Finding solution of the system of equations\n ");
  printf("The solution is found to be [x;y]= \n");
  print_vector(x);
  printf("The number of iterations for the root finding is %i and number of funtions calls is %i\n\n",iter,ncalls);

  // rosenbrock function
  ncalls = 0;
  gsl_vector_set(x,0,2.0);
  gsl_vector_set(x,1,2.0);
  iter = newton_with_jacobian(rosenbrock_jac,x,eps);
  printf("Finding the minimum of the Rosenbrock function\n ");
  printf("The minimum is found to be [x;y]= \n");
  print_vector(x);
  printf("The number of iterations for the root finding is %i and number of funtions calls is %i\n\n",iter,ncalls);

  // himmelblau function
  ncalls = 0;
  gsl_vector_set(x,0,4.0);
  gsl_vector_set(x,1,4.0);
  iter = newton_with_jacobian(himmelblau_jac,x,eps);
  printf("Finding the minimum of the himmelblau function\n ");
  printf("The minimum is found to be [x;y]= \n");
  print_vector(x);
  printf("The number of iterations for the root finding is %i and number of funtions calls is %i\n\n",iter,ncalls);


  gsl_vector_free(x);

}

void test_b(){
  gsl_vector* x = gsl_vector_alloc(2);
  double eps = 1e-6, dx = 1e-6;
  int iter;


  printf("\nTesting rooting finding with jacobian calculated nummerically\n\n");

  // system of equuations given by dmitri
  ncalls = 0;
  gsl_vector_set(x,0,0);
  gsl_vector_set(x,1,1);
  iter = newton_jacobian_num(f,x,dx,eps);
  printf("Finding solution of the system of equations\n ");
  printf("The solution is found to be [x;y]= \n");
  print_vector(x);
  printf("The number of iterations for the root finding is %i and number of funtions calls is %i\n\n",iter,ncalls);

  // rosenbrock function
  ncalls = 0;
  gsl_vector_set(x,0,2.0);
  gsl_vector_set(x,1,2.0);
  iter = newton_jacobian_num(rosenbrock,x,dx,eps);
  printf("Finding the minimum of the Rosenbrock function\n ");
  printf("The minimum is found to be [x;y]= \n");
  print_vector(x);
  printf("The number of iterations for the root finding is %i and number of funtions calls is %i\n\n",iter,ncalls);

  // himmelblau function
  ncalls = 0;
  gsl_vector_set(x,0,4.0);
  gsl_vector_set(x,1,4.0);
  iter = newton_jacobian_num(himmelblau,x,dx,eps);
  printf("Finding the minimum of the himmelblau function\n ");
  printf("The minimum is found to be [x;y]= \n");
  print_vector(x);
  printf("The number of iterations for the root finding is %i and number of funtions calls is %i\n\n",iter,ncalls);

  // Calculate roots with gsl rutine
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_dnewton;
	gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,2);

	gsl_multiroot_function F;

  // himmelblau
	F.f = himmelblau_gsl;
	F.n = 2;
	F.params = NULL;

  gsl_vector_set(x,0,4.0);
  gsl_vector_set(x,1,4.0);
	gsl_multiroot_fsolver_set(S,&F,x);

	int flag;
	iter = 0;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,eps);
	}while(flag!=GSL_SUCCESS);

	printf("Using GSL routine, one finds the solutions for himmelblau's function\n");
	print_vector(S->x);
	printf("in %i iterations\n",iter);

  // rosenbrock
	F.f = rosenbrock_gsl;
	F.n = 2;
	F.params = NULL;

  gsl_vector_set(x,0,2.0);
  gsl_vector_set(x,1,2.0);
	gsl_multiroot_fsolver_set(S,&F,x);

	iter = 0;
  int flag1;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag1 = gsl_multiroot_test_residual(S->f,eps);
	}while(flag1!=GSL_SUCCESS);

	printf("\nUsing GSL routine, one finds the solutions for rosenbrock's function\n");
	print_vector(S->x);
	printf("in %i iterations\n",iter);

  gsl_multiroot_fsolver_free(S);
  gsl_vector_free(x);

}

int main(){
  test_a();
  test_b();
  return 0;
}
