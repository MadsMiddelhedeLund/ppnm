#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"root_finding.h"
/*
int newton(void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double eps){
	int n=x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	while(1){
		f(x,fx);
		for (int j=0;j<n;j++){
			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
			f(x,df);
			gsl_vector_sub(df,fx); // df=f(x+dx)-f(x)
			for(int i=0;i<n;i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
			}
		qrdec(J,R);
		qrsolve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
		double s=1;
		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,fz);
			if( gsl_blas_dnrm2(fz)<(1-s/2)*gsl_blas_dnrm2(fx) || s<0.02 ) break;
			s*=0.5;
			gsl_vector_scale(Dx,0.5);
			}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
		if( gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(fx)<eps ) break;
		}
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_vector_free(df);
	gsl_vector_free(Dx);

	return 1;
}
*/
int newton_with_jacobian(void f(gsl_vector* x,gsl_vector* fx, gsl_matrix* J ),gsl_vector* x, double eps){
	int n = x->size;
	int iter = 0;
	int iter_max = 1e6;
	double lambda, fx_norm, fz_norm;

	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	do{
		f(x,fx,J);
		fx_norm = gsl_blas_dnrm2(fx);
		// solve for DX
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);

		lambda = 1.;

		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,fz,J);
			fz_norm = gsl_blas_dnrm2(fz);
			if( fz_norm<(1-lambda/2)*fx_norm || lambda<1./64 ) break;
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5);
		}
		iter++;
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
	}while(fx_norm > eps && iter<iter_max);


	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(Dx);

	return iter;
}


void jacobian(void f(gsl_vector* x,gsl_vector* fx),gsl_matrix* J, gsl_vector* x, gsl_vector* fx, gsl_vector* df, double dx, int n){
	for (int j=0;j<n;j++){
		gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
		f(x,df);
		gsl_vector_sub(df,fx); // df=f(x+dx)-f(x)
		for(int i=0;i<n;i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
		gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
		}
}
int newton_jacobian_num(void f(gsl_vector* x,gsl_vector* fx),gsl_vector* x, double dx, double eps){
	int n = x->size;
	int iter = 0;
	int iter_max = 1e6;
	double lambda, fx_norm, fz_norm;

	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);

	do{
		f(x,fx);
		fx_norm = gsl_blas_dnrm2(fx);
		jacobian(f,J,x,fx,df,dx,n);
		// solve for DX
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);

		lambda = 1.;

		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,fz);
			fz_norm = gsl_blas_dnrm2(fz);
			if( fz_norm<(1-lambda/2)*fx_norm || lambda<1./64 ) break;
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5);
		}
		iter++;
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
	}while(fx_norm > eps && iter<iter_max);


	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(Dx);
	gsl_vector_free(df);

	return iter;
}
