#include<stdio.h>
#include"nvector.h"
#include "stdlib.h"
#include"math.h"
nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free((*v).data); free(v);}

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

double   nvector_dot_product (nvector* u, nvector* v){
  double result;
  if((*u).size==(*v).size){
    for(int i =0;i<(*u).size;i++){
      result += nvector_get(v,i)*nvector_get(u,i);
    }
  }
  else {
    printf("Vectors must be of the same size\n");
  }
return result;
}

void nvector_print    (char* s, nvector* v) {
  printf("%s\n",s);
  for (int i=0;i<(*v).size;i++) printf("%g\n",nvector_get(v,i));
}

void nvector_set_zero (nvector* v){
  for (int i = 0; i<(*v).size;i++) nvector_set(v,i,0);
}

int double_equal(double a, double b){
	double TAU = 1e-6, EPS = 1e-6;
	if (fabs(a - b) < TAU)
		return 1;
	if (fabs(a - b) / (fabs(a) + fabs(b)) < EPS / 2)
		return 1;
return 0;
}

int  nvector_equal    (nvector* a, nvector* b){
  if ((*a).size != (*b).size) return 0;
  for (int i=0;i<(*a).size;i++){
    if (!double_equal((*a).data[i],(*b).data[i])) return 0;
  }
  return 1;
}

void nvector_add      (nvector* a, nvector* b){
  if((*a).size==(*b).size){
    for (int i=0;i<(*a).size;i++)
      nvector_set(a,i,(*a).data[i]+(*b).data[i]);
  }
}

void nvector_sub      (nvector* a, nvector* b){
  if((*a).size==(*b).size){
    for (int i=0;i<(*a).size;i++)
      nvector_set(a,i,(*a).data[i]-(*b).data[i]);
  }
}

void nvector_scale    (nvector* a, double x){
  for (int i=0;i<(*a).size;i++) nvector_set(a,i,x*(*a).data[i]);
}
