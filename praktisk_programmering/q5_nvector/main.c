#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

  printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
  if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

  printf("\nmain: testing nvector_set and nvector_get ...\n");
  nvector_set(v,1,2);
  printf("%g\n",nvector_get(v,1));

  printf("\nmain: test nvector_dot_product...\n");
  nvector *u = nvector_alloc(n);
  nvector_set(u,1,2);
  double dotProduct = nvector_dot_product(u,v);
  printf("u dot v=%g\n",dotProduct);

  printf("\nmain: test nvector_print...\n");
  nvector_print("this the vector v",v);

  printf("\nmain: test nvector_set_zero...\n");
  nvector_set_zero(v);
  nvector_print("this the vector v",v);
  nvector_print("this the vector u",u);

  printf("\nmain: test nvector_equal ...\n");
  int equal_test = nvector_equal(u,v);
  printf("true/false = %i\n",equal_test);

  printf("\nmain: test nvector_add\n");
  nvector_add(v,u);
  nvector_print("this is v+u",v);

  printf("\nmain: test nvector_sub\n");
  nvector_sub(v,u);
  nvector_print("this is v-u",v);

  printf("\nmain: test nvector_scale\n");
  double scale = 3;
  nvector_set(v,1,2);
  nvector_set(v,2,3);
  nvector_print("v before scale",v);
  nvector_scale(v,scale);
  printf("now we scale v by %g\n",scale);
  nvector_print(" ",v);




	return 0;
}
