#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_sf_airy.h>
#include<math.h>

int main(){
  for (double x=-20; x<5;x+=0.1)
    printf("%g %g %g \n",x,gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE),
    gsl_sf_airy_Bi(x,GSL_PREC_DOUBLE));
return 0;
}
