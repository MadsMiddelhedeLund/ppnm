#include<stdio.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<stdlib.h>
void matrix_print(const char* s,const gsl_matrix* m){
	printf(s); printf("\n");
	for(int i=0;i<m->size2;i++){
		for(int j=0;j<m->size1;j++)
			printf("%8.3g ",gsl_matrix_get(m,i,j));
		printf("\n");
		}
	}
void vector_print(const char* s,const gsl_vector* v){
	printf(s); printf("\n");
	for(int i=0;i<v->size;i++)
		printf("%8.3g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main(){
  gsl_vector *b = gsl_vector_calloc(3);

  gsl_vector_set(b,0,6.23);
  gsl_vector_set(b,1,5.37);
  gsl_vector_set(b,2,2.29);

  gsl_matrix *A = gsl_matrix_calloc(3,3);

	FILE* mystream =fopen("linData.txt","r");
	gsl_matrix_fscanf(mystream,A);
	fclose(mystream);

  /*matrix and vector copy*/
  gsl_matrix *A_copy = gsl_matrix_calloc(3,3);
  gsl_matrix_memcpy(A_copy,A);

  matrix_print("A = ",A_copy);
  vector_print("b = ",b);

  gsl_vector *x = gsl_vector_calloc(3);

  /*solving the linar equations*/
  gsl_linalg_HH_solve(A_copy,b,x);
  vector_print("solution x to Ax=b :",x);
  /* check if this is correct*/
  gsl_vector *y = gsl_vector_calloc(3);
  gsl_blas_dgemv(CblasNoTrans,1,A,x,0,y);
	vector_print("check:  Ax= (should be equal b)",y);

  gsl_vector_free(y);
  gsl_vector_free(x);
  gsl_vector_free(b);
  gsl_matrix_free(A_copy);
  gsl_matrix_free(A);
return 0;
}
