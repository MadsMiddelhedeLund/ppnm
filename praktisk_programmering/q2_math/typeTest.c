#include <stdio.h>

int main(){
  printf("\n\n Significant digits - Exercise\n");
  int         n1 = 0.11111111111111111111111111111;
  double      n2 = 0.11111111111111111111111111111;
  long double n3 = 0.11111111111111111111111111111L;
  float       n4 = 0.11111111111111111111111111111;
  printf("integer %d\n",n1 );
  printf("double %.25g\n",n2);
  printf("long double %0.25Lf\n",n3);
  printf("float %.25f\n",n4);
  return 0;
}
