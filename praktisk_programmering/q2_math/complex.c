#include <stdio.h>
#include <complex.h>
#include <math.h>
int main(){
  printf("\n\nComplex Numbers - Exercise\n");
  double complex n1 = csqrt(-2);
  printf("square root of -2  %.2f%+.2fi\n",creal(n1),cimag(n1));
  double complex n2 = cexp(-I);
  printf("exp(-i)=%.2f%+.2fi\n",creal(n2),cimag(n2));
  double complex n3 = cexp(-I*M_PI);
  printf("exp(-i*pi)=%.2f%+.2fi\n",creal(n3),cimag(n3));
  double complex n4 = cpow(I,M_E);
  printf("I^e=%.2f%+.2fi\n",creal(n4),cimag(n4));
  return 0;
}
