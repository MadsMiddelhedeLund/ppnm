#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include "equal.h"
#include "nameDigit.h"
void intMaxDW();
void intMinDW();
void intMaxF();
void intMinF();
void macEpsD();
void macEpsF();
void macEpsLD();
void sumUp();
void sumDown();
void sumUpD();
void sumDownD();

int main(){
  printf("Exercise 1\n");
  intMaxDW();
  intMinDW();
  intMaxF();
  intMinF();
  macEpsD();
  macEpsF();
  macEpsLD();
  printf("Exercise 2\n");
  sumUp();
  sumDown();
  sumUpD();
  sumDownD();
  printf("Exercise 3\n");
  int x =equal(1,2,3,4);
  printf("%i\n",x);
  printf("Exercise 4\n");
  nameDigit(100);
return 0;
}

void intMaxDW(){
  int i = 1;
  do i++;while(i+1>i);
  printf("do while my max int = %i\n",i);
  printf("INT_MAX=%i\n",INT_MAX);
}

void intMinDW(){
  int i = 1;
  do i --;while(i-1<i);
  printf("do while my min int = %i\n",i);
  printf("INT_MIN=%i\n",INT_MIN);
}

void intMaxF(){
  int a;
  for (int i=1;i+1>i;i++)a=i;
  printf("for my max int = %i\n",a);
  printf("INT_MAX=%i\n",INT_MAX);
}

void intMinF(){
  int a;
  for (int i=1;i-1<i;i--)a=i;
  printf("for my min int = %i\n",a);
  printf("INT_MIN=%i\n",INT_MIN);
}

void macEpsD(){
  double x = 1;
  while(1+x!=1)x/=2;
  x*=2;
  printf("double smallest epsilon= %g\n",x);
  printf("DBL_EPSILON=%g\n",DBL_EPSILON);
}

void macEpsF(){
  float x = 1;
  while(1+x!=1)x/=2;
  x*=2;
  printf("float smallest epsilon = %g\n",x);
  printf("FLT_EPSILON=%g\n",FLT_EPSILON);
}

void macEpsLD(){
  long double x = 1;
  while(1+x!=1)x/=2;
  x*=2;
  printf("float smallest epsilon = %Lg\n",x);
  printf("LDBL_EPSILON=%Lg\n",LDBL_EPSILON);
}

void sumUp(){
  int max = INT_MAX/2;
  float x = 0;
  int i=1;
  while(i<=max){
    x+=1.0f/i;
    i++;
  }
  printf("sum up float =%g\n",x);
}

void sumDown(){
  int max = INT_MAX/2;
  float x = 0;
  int i = max;
  while(i>=1){
    x+=1.0f/(i);
    i--;
  }
  printf("sum down float=%g\n",x);
}

void sumUpD(){
  int max = INT_MAX/2;
  double x = 0;
  int i=1;
  while(i<=max){
    x+=1.0f/i;
    i++;
  }
  printf("sum up double =%g\n",x);
}

void sumDownD(){
  int max = INT_MAX/2;
  double x = 0;
  int i = max;
  while(i>=1){
    x+=1.0f/(i);
    i--;
  }
  printf("sum down double =%g\n",x);
}
