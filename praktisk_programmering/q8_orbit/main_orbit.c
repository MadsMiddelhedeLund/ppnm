#include<stdio.h>
#include<unistd.h> /* short comman-line options */
#include<getopt.h> /* long and short comman-line options */
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int diff_eq(double x, const double y[],double f[],void *params){
  double epsilon = *(double *) params;
  f[0] = y[1];
  f[1] = 1-y[0]+epsilon*y[0]*y[0];
return GSL_SUCCESS;
}

int main(){
  gsl_odeiv2_system sys;
  sys.function = diff_eq;
  sys.jacobian = NULL;
  sys.dimension = 2;
  /* set params later in loop*/


  /*driver(stepper,controler and evoler)*/
  double acc = 1e-8;
  double eps = 1e-10;
  double hstart = 1e-6;
  gsl_odeiv2_driver* driver =
		gsl_odeiv2_driver_alloc_y_new
			(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

  /* initial conditions and parameters*/
  double y[2];
  y[0] = 1;
  double y_prime_val[3] = {0.0,-0.5,-0.49};
  double epsilon_vals[3] = {0.0,0.0,0.01};
  double x_min = 0;
  double x_max_val[3] = {2*M_PI,2*M_PI,50*M_PI}; /* circular and elliptical is the same from 2pi*/
  double x; double x_max;
  double x_step=0.01;

  for(int i = 0; i<3;i++){
    sys.params = &epsilon_vals[i];
    y[1] = y_prime_val[i];
    x_max = x_max_val[i];
    x = x_min;

    printf("# epsilon=%g\n",epsilon_vals[i]);

    for(double x_next = x_min+x_step;x_next<=x_max;x_next+=x_step){
      gsl_odeiv2_driver_apply(driver,&x,x_next,y);
      printf("%g %g\n",x_next,y[0]);
    }

		printf("\n\n");
  }

  gsl_odeiv2_driver_free(driver);
return 0;
}
