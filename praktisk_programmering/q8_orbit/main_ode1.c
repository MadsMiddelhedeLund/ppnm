#include<stdio.h>
#include<unistd.h> /* short comman-line options */
#include<getopt.h> /* long and short comman-line options */
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int diff_eq(double x, const double y[],double dydx[],void *params){
  dydx[0] = y[0]*(1-y[0]);
return GSL_SUCCESS;
}

int main(){
  gsl_odeiv2_system sys;
  sys.function = diff_eq;
  sys.jacobian = NULL;
  sys.dimension = 1;
  sys.params = NULL;


  /*driver(stepper,controler and evoler)*/
  double acc = 1e-8;
  double eps = 1e-10;
  double hstart = 1e-6;
  gsl_odeiv2_driver* driver =
		gsl_odeiv2_driver_alloc_y_new
			(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

  /*initial, final and stepping conditions*/
  double x_min = 0;
  double x_max = 3;
  double x_step = 0.01;
  double y[1];
  y[0]=0.5;
  double x = x_min;

  /* finding the y values*/
  for(double x_next = x_min+x_step;x_next<=x_max;x_next += x_step){
    gsl_odeiv2_driver_apply(driver,&x,x_next,y);
    printf("%g %g %g\n",x_next,y[0],1/(1+exp(-x_next)));
  }

  gsl_odeiv2_driver_free(driver);
return 0;
}
