#include <stdio.h>
#include <gsl/gsl_multiroots.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <stdlib.h>
int f_root(const gsl_vector* a,void* params, gsl_vector* f ){
  const double a0 = gsl_vector_get(a,0);
  double x = *(double*) params;
  const double f_a = tan(a0)-x;

  gsl_vector_set(f,0,f_a);

return GSL_SUCCESS;
}

double my_atan(double x){
  const gsl_multiroot_fsolver_type * T = gsl_multiroot_fsolver_hybrids;
  const size_t n = 1;
  gsl_multiroot_fsolver * s = gsl_multiroot_fsolver_alloc(T,n);
  gsl_multiroot_function F = {&f_root,n,&x};

  gsl_vector *a = gsl_vector_alloc(n); // i call the variable a, because that is what we are varying in the problem
  gsl_vector_set(a,0,1);

  int status;
  int iter = 0;
  double epsabs = 1e-8;

  gsl_multiroot_fsolver_set(s,&F,a);

  do{
    iter ++;
    status = gsl_multiroot_fsolver_iterate(s);
	fprintf(stderr,"iter=%i, status=%i\n",iter,status);

    if (status) break;

    status = gsl_multiroot_test_residual(s->f,epsabs);
	fprintf(stderr,"status-test-residual=%i, GSL_CONTINUE=%i\n",status,GSL_CONTINUE);
	double f=gsl_vector_get(s->f,0);
	double x=gsl_vector_get(s->x,0);
    printf("iter=%i, x=%g, f=%g\n",iter,x,f);
  }while(status == GSL_CONTINUE && iter < 1000);

  double result = gsl_vector_get(s->x,0);

  gsl_vector_free(a);
  gsl_multiroot_fsolver_free(s);

  return result;
}

int main() {
  FILE* file = fopen("data.txt","w");
  fprintf(file,"#x atan(x)\n");
  for (double x = -1; x<=1;x+=0.01)
  fprintf(file,"%g %g\n",x,my_atan(x));
  fclose(file);

return EXIT_SUCCESS;
}
