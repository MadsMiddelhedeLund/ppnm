#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>

double Fe(double e,double r);

int M(const gsl_vector* x, void* params,gsl_vector* f ){
  double e = gsl_vector_get(x,0);
  assert(e<0); // e has to be negative
  double rmax = *(double*) params;
  double fval = Fe(e,rmax);
  gsl_vector_set(f,0,fval);
return GSL_SUCCESS;
}

int main(){

    double rmax=8;
    double upper_limit=100;
    const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_broyden; // the type dmitri gsl_multiroot_fsolver_set
    const size_t n = 1; // one equations
    gsl_multiroot_fsolver * s= gsl_multiroot_fsolver_alloc(T,n);
    gsl_multiroot_function F = {&M, n,&rmax};

    gsl_vector *x = gsl_vector_alloc(n);
    gsl_vector_set(x,0,-1); // e=-1 start quess of energy

    gsl_multiroot_fsolver_set(s,&F,x);

    FILE* file = fopen("shooting.dat","w");

    int status, iter=0;
    const double epsabs = 1e-3;
    do {
      iter++;
      status = gsl_multiroot_fsolver_iterate(s);
      if(status) break;

      status = gsl_multiroot_test_residual(s->f,epsabs);
      printf("iter= %3i ",iter);
      printf("e= %10g ",gsl_vector_get(s->x,0));
      printf("f(rmax)= %10g ",gsl_vector_get(s->f,0));
      printf("\n");
      if(status==GSL_SUCCESS)printf("converged\n"); // only doing this to say is has converged.
    }while(status == GSL_CONTINUE && iter<upper_limit);

    double e = gsl_vector_get(s->x,0); // getting the result, energy

    // making the values for the plot
    printf("E=%g\n",e);
    fprintf(file,"shooting method\n");
    for (double r=0; r<rmax+1e-5;r+=rmax/100)
      fprintf(file,"%g %g\n",r,Fe(e,r));
    fprintf(file,"\n\nexact\n");
    for (double r=0; r<rmax+1e-5;r+=rmax/100)
      fprintf(file,"%g %g\n",r,r*exp(-r));



    fclose(file);
    gsl_vector_free(x);
    gsl_multiroot_fsolver_free(s);
return 0;
}
