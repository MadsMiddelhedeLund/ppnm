#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>

int SE(double x, const double y[],double dydx[],void *params){
  double e = *(double*) params;
  dydx[0] = y[1];
  dydx[1] = 2*(-e-1/x)*y[0];
return GSL_SUCCESS;
}

double Fe(double e, double r){

  double rmin = 1e-3;
  assert(r>=0); // making sure r=>0
  if(r<rmin) return r-r*r;

  gsl_odeiv2_system sys;
  sys.function = SE;
  sys.jacobian = NULL;
  sys.dimension = 2;
  sys.params = (void*)&e;

  double acc = 1e-6;
  double eps = 1e-6;
  double hstart = 1e-3;
  gsl_odeiv2_driver* driver =
    gsl_odeiv2_driver_alloc_y_new
      (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

  // initial values/ boundaries
  double t = rmin;
  double y[] = {t-t*t,1-2*t};

  // solveing the differential equations
  gsl_odeiv2_driver_apply(driver,&t,r,y);

  gsl_odeiv2_driver_free(driver);
  return y[0];
}
