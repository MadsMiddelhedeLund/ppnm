#include <stdio.h>
#include <gsl/gsl_multiroots.h>
#include <math.h>

int grad_rosenbrock(const gsl_vector* x, void* params,gsl_vector* f){
  const double x0 = gsl_vector_get(x,0); // instead of x and y
  const double x1 = gsl_vector_get(x,1);

  const double dfx0 = -2*(1-x0)-400*(x1-x0*x0)*x0;
  const double dfx1 = 200*(x1-x0*x0);

  gsl_vector_set(f,0,dfx0);
  gsl_vector_set(f,1,dfx1);

return GSL_SUCCESS;
}

int main(){
  const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
  const size_t n = 2; // two equations
  gsl_multiroot_fsolver * s = gsl_multiroot_fsolver_alloc(T,n);
  gsl_multiroot_function F = {&grad_rosenbrock,n,NULL};

  gsl_vector *x = gsl_vector_alloc(n); // initializing the x vector
  gsl_vector_set(x,0,0);
  gsl_vector_set(x,1,0);

  int status;
  int iter = 0;
  double epsabs = 1e-8;

  gsl_multiroot_fsolver_set(s,&F,x);

  do {
    iter ++;
    status = gsl_multiroot_fsolver_iterate(s);

    if (status) break;

    status = gsl_multiroot_test_residual(s->f,epsabs);
    printf("(%g,%g)\n",gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));
  } while(status == GSL_CONTINUE && iter < 1000);

  printf("number of interations: %i\n",iter);

  gsl_multiroot_fsolver_free(s);
  gsl_vector_free(x);

return 0;
}
