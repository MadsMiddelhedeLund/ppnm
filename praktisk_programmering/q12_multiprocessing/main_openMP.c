#include<stdio.h>
#include<stdlib.h>
#include<omp.h>

int main(int argc, char** argv){
  int n = argc>1? (int) atof(argv[1]):(int)1e6;
  int n_in1 = 0;
  int n_in2 = 0;
  int n_in = 0;
  uint seed1 = 1;
  uint seed2 = 42;

/*  double x1,y1,r1,x2,y2,r2; */
  int num_par = 2;
//  #pragma omp parallel sections reduction(+:n_in)
  #pragma omp parallel sections
  {
    #pragma omp section
    {
	int* n_in = &n_in1;
      for (int i=0;i<n;i++) {
        double x1 = (double) rand_r(&seed1)/ RAND_MAX;
        double y1 = (double) rand_r(&seed1)/ RAND_MAX;
        double r1 = x1*x1+y1*y1;
        if (r1<1) (*n_in)++;
      }
    }
    #pragma omp section
    {
	int* n_in = &n_in2;
      for (int i=0;i<n;i++) {
        double x2 = (double) rand_r(&seed2)/ RAND_MAX;
        double y2 = (double) rand_r(&seed2)/ RAND_MAX;
        double r2 = x2*x2+y2*y2;
        if (r2<1) (*n_in)++;
      }
    }
  }

  double p = (double) (n_in1+n_in2)/(num_par*n);
//  double p = (double) n_in/(num_par*n);
  double result = 4*p;
  printf("(n_in1,n_in2)= (%i,%i)\nestimated pi = %g\n",n_in1,n_in2,result);


return EXIT_SUCCESS;
}
