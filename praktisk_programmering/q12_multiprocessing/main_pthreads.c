#include<pthread.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
struct params {int N,N_in; uint seed;};

void* bar(void* param){
  double x,y,r;
  struct params* data=(struct params*) param;
  int N = data->N;
  for (int i=0;i<N;i++) {
    x = (double) rand_r(&(data->seed))/ RAND_MAX;
    y = (double) rand_r(&(data->seed))/ RAND_MAX;
    r = sqrt(x*x+y*y);
    if (r<1) data->N_in++;
  }
return NULL;
}

int main(int argc, char** argv){
	int N=argc>1?(int)atof(argv[1]):1000000;
  pthread_t thread1;
  pthread_t thread2;

  struct params data1,data2;
  data1.N = N;
  data1.N_in = 0;
  data1.seed = 1;
  data2.N = N;
  data2.N_in = 0;
  data2.seed = 42;

  pthread_create(&thread1,NULL,bar,(void*)&data1);
  pthread_create(&thread2,NULL,bar,(void*)&data2);

  pthread_join(thread1,NULL);
  pthread_join(thread2,NULL);
  int N_in1 = data1.N_in;
  int N_in2 = data2.N_in;
  double p = (double)(data1.N_in + data2.N_in)/(double)(data1.N+data2.N);
  double result = 4*p;
  printf("(N_in1,N_in2) = (%i,%i)\nresult = %g\n",N_in1,N_in2,result);
return 0;
}
