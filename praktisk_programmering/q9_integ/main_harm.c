#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double norm_int(double x, void * params){
  double alpha = *(double *)params;
  double f = exp(-alpha*x*x);
  return f;
}

double harm_osc(double x,void * params){
  double alpha = *(double *)params;
  double f = (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*exp(-alpha*x*x);
  return f;
}

int main(){

  // variables
  double alpha,result_norm,result_ham,error_norm,error_ham;
  double epsabs = 1e-8;
  double epsrel = 1e-8;
  int limit = 100;
  gsl_integration_workspace * w = gsl_integration_workspace_alloc(limit);

  FILE* file = fopen("harm.dat","w");

  // norm integral made ready
  gsl_function F_norm_int;
  F_norm_int.function = & norm_int;
  F_norm_int.params = (void*)& alpha;

  // Hamiltonian integral
  gsl_function F_harm_osc;
  F_harm_osc.function = & harm_osc;
  F_harm_osc.params = (void*)& alpha;

  for(int i=10;i<1000;i++){
    alpha = (double) i/100.0;
    gsl_integration_qagi(&F_norm_int,epsabs,epsrel,limit,w,&result_norm,&error_norm);
    gsl_integration_qagi(&F_harm_osc,epsabs,epsrel,limit,w,&result_ham,&error_ham);
    fprintf(file,"%g %g\n", alpha, result_ham/result_norm);
  }
  fclose(file);
  gsl_integration_workspace_free(w);

return 0;
}
