#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double f(double x, void* params){
  double f = log(x)/sqrt(x);
  return f;
}

int main(){

  double result, error;
  double epsabs = 0;
  double epsrel = 1e-7;
  double lower  = 0;
  double upper  = 1;
  int limit = 1000;

  gsl_integration_workspace * w = gsl_integration_workspace_alloc(limit);

  gsl_function F;
  F.function = &f;
  F.params = NULL;


  gsl_integration_qags (&F,lower,upper,epsabs,epsrel,limit,w,&result,&error);

  printf ("result          = % .18f\n", result);
  printf ("estimated error = % .18f\n", error);
  printf ("intervals       = %zu\n", (*w).size);

  gsl_integration_workspace_free (w);

return 0;
}
