#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<stdlib.h>

typedef struct {int n; double* t,*y,*e;} exp_data;


double least_square (const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);
	exp_data data = *(exp_data*) params;
	int     n = data.n;
	double *t = data.t;
	double *y = data.y;
	double *e = data.e;
	double sum=0;
	double g(double t){return A*exp(-(t)/T) + B;}
	for(int i=0;i<n;i++){
    sum += pow((g(t[i]) - y[i])/e[i],2);
  }
	return sum;
}

int main(){
  double t[] = {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[] = {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[] = {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);

  exp_data data;
  data.n = n;
  data.t = t;
  data.y = y;
  data.e = e;

  size_t num_vari = 3;

  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(T,num_vari);
  gsl_multimin_function F;
  F.f = &least_square;
  F.n = num_vari;
  F.params = (void*)&data;

  gsl_vector * x = gsl_vector_alloc(num_vari);
  gsl_vector_set(x, 0, 5);
  gsl_vector_set(x, 1, 3);
  gsl_vector_set(x, 2, 1);

  // initial step size
  double ini_step   = 0.01;
  gsl_vector *step  = gsl_vector_alloc(num_vari);
  gsl_vector_set(step,0,ini_step);
  gsl_vector_set(step,1,ini_step);
  gsl_vector_set(step,2,ini_step);

  gsl_multimin_fminimizer_set(s,&F,x,step);

  int iter = 0;
  int status;
  double epsabs = 1e-6;
  double size;
  int iter_max = 1000;

  printf("Function evaluated at:\n");
  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status) break;

      status = gsl_multimin_test_size(s->size,epsabs);

      // printing x and y(the path)
      printf("(%g,%g,%g)\n",gsl_vector_get(s->x,0),gsl_vector_get(s->x,1),gsl_vector_get(s->x,2));
    }while(status == GSL_CONTINUE && iter < iter_max);

  if(iter == iter_max) printf("Minimizer did not converge after %i iterations\n", iter_max);
  else printf("Minimizer converged after %i iterations\n",iter);
  double A_fit = gsl_vector_get(s->x,0);
	double T_fit = gsl_vector_get(s->x,1);
	double B_fit= gsl_vector_get(s->x,2);
  printf("Minimum found at (A,T,B)=(%g,%g,%g)\n",A_fit,T_fit,B_fit);

	//making data to plot
	FILE* file = fopen("dataLS.dat","w");
	for(int i=0; i<n; i++){
		fprintf(file,"%g %g %g\n", t[i], y[i], e[i]);
	}

	double f(double x){return A_fit*exp(-x/T_fit) + B_fit;}

	fprintf(file,"\n\n");
	for(double i=t[0]-1; i<t[n-1]+1; i+=0.01){
		fprintf(file,"%g %g\n", i, f(i));
	}


	fclose(file);
  gsl_multimin_fminimizer_free(s);
  gsl_vector_free(x);
  gsl_vector_free(step);

return 0;
}
