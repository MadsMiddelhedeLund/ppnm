#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<stdlib.h>

double rosenbrock(const gsl_vector * x, void * params){
  const double x0 = gsl_vector_get(x,0);
  const double x1 = gsl_vector_get(x,1);

  double y = (1-x0)*(1-x0)+100*(x1-x0*x0)*(x1-x0*x0);
  return y;

}

int main(){
  const size_t n = 2; // dimension 2 because it has variable x and y

  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(T,n);
  gsl_multimin_function F;
  F.f = &rosenbrock;
  F.n = n;
  F.params = NULL;

  // setting the vector.
  gsl_vector *x = gsl_vector_alloc(n); // must hold x and y
  gsl_vector_set(x,0,0.5); // we know the minima is at (1,1)
  gsl_vector_set(x,1,0.5);

  // initial step size
  double ini_step   = 0.01;
  gsl_vector *step  = gsl_vector_alloc(n);
  gsl_vector_set(step,0,ini_step);
  gsl_vector_set(step,1,ini_step);

  gsl_multimin_fminimizer_set(s,&F,x,step);

  FILE* file = fopen("path.dat","w");

  int iter = 0;
  int status;
  double epsabs = 1e-6;
  double size;
  int iter_max = 1000;
  printf("Function evaluated at:\n");
  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status) break;

      status = gsl_multimin_test_size(s->size,epsabs);

      // printing x and y(the path)
      printf("(%g,%g)\n",gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));
      fprintf(file,"%g %g\n",gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));
    }while(status == GSL_CONTINUE && iter < iter_max);

    if(iter == iter_max) printf("Minimizer did not converge after %i iterations\n", iter_max);
    else printf("Minimizer converged after %i iterations\n",iter);
    printf("Minimum found at (%g,%g)\n",gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));

  fclose(file);
  gsl_multimin_fminimizer_free(s);
  gsl_vector_free(x);
  gsl_vector_free(step);
return 0;
}
